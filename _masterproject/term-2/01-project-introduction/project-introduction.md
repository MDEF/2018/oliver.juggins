---
title: Project Overview
period: # 04 October 2018 - 19 December 2018
date: 2019-01-24 12:00:00
term: 2
published: true
---
  <!-- Talk about what has happened so far term 2. How I need to revisit speculation more and revisit the bigger issues Re-read this and put video-->

  <iframe src="https://player.vimeo.com/video/331261904" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

  My project starts from the premise that society should be well equipped with the relevant skills and knowledge to understand new technology in order to be able to shape it effectively and not be controlled by it. Artificial intelligence (AI) and machine learning (ML) are technologies that have become a ubiquitous part of daily life and underpin much of our digital infrastructure but are fields that are not generally well understood outside of the world of computer science.

  Much of the current literature for learning about AI is aimed at programmers, focusing on highly technical knowledge within the realm of data science. As a result, my project is to offer alternative modes of learning about AI, focusing on the basic principles which require no coding experience requiring no coding using analogy and metaphor to convey ideas. Not everyone needs to learn how convolutional neural networks work, but the ethical questions raised by autonomous weapons, driverless cars and the increased presence of intelligent machines in daily life are issues which affect everyone.

  So how can I engage people with AI and create interest in the relevant issues society faces by this technology? My own interest and journey of discovery with AI was aided through relating it to the arts and looking at artists who had used AI in the production of their work and addressed the difficult questions that this technology raises. Through the creative re-appropriation of AI, I aim to make the subject more accessible and demystify terms that many might have heard but not fully understood, using the visual arts and music to create interactive and playful experiences that make use of open source hardware and software in their development and delivery.

  My project is primarily aimed at empowering a younger audience between the ages of 10 and 15 to learn about and create with AI. I believe this is an important topic for children to learn about as AI is technology that they are growing up with, and intelligent machines are becoming an increasingly significant part of daily life so interacting with them and understanding them will be an important skill to develop. I plan to use Barcelona as my testing ground, working within the Fab Lab Barcelona with the Future Learning Unit. As such the project will serve as a series of workshops focused around creating interactive and playful experiences that make use of open source hardware and software in their development and delivery.

  The project's ultimate aim is to take AI out of the ‘black box’ and present it as a subject which is fundamental knowledge with large implications for society, but also as an enabling and emergent technology that can positively further human practice and creativity. By doing this I hope to engage with some of the wider societal issues facing AI such as lack of diversity in its development, algorithmic bias and contribute to the discourse framed within the ‘democratic AI’ movement.

  The different elements within the masters project section of the website will record the progress made with my project where there are state of the art references, reading and a series of reflections that form part of the design studio.
