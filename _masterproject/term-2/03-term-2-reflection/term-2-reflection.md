---
title: Term 2 Reflections
period: # 04 October 2018 - 19 December 2018
date: 2019-01-24 12:00:00
term: 2
published: true
---
<!-- Include 5 slide presentation and Indy Johar feedback presentation, and update Mi-d term reflections, and add to google drive

Include end of term presentation stuff here -->

## Mid-Term Review

<br>
<div data-configid="35105822/68383070" style="width:640px; height:453px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>
<br>

Throughout the term as part of the design studio as a class we have been presenting our work and discussing ideas about our projects and wider themes that invited guests have introduced to us through their talks. The mid-term review was a good chance to try and tie all of the work done so far and distil it into a five minute presentation, which was challenging but a useful constraint. Having only five minutes meant that only the most important points about the project and story could be included, so was a good exercise in trying to be concise as well.

As I am not a very confident speaker when doing presentations so I found the exercise really useful to get more practice in presenting my work. I feel that I could have presented a lot better but now have a sense of how I can present my work a lot more successfully at the end of term review. There are a number of reasons for this, but one of them is the format and content of the presentation itself. My slides had too much text, and visually were not that interesting to look at. Writing the text has been really useful for me personally, and it is useful to have my ideas and story written down, but would be better placed in a document rather than a presentation at this stage.

Next time my presentation will have a  more consistent style using keywords and relevant images to emphasize the most important points to the audience and not overwhelm them with information. There were others in the class who had really nicely put together presentations so it was good to not only learn about everyone's projects but also get some inspiration on presenting and putting together presentations which are more engaging and interesting to look at.

I feel that since returning from China I have had an idea forming about what I would like my project to be, but progress has been a little slow. The mid-term review was a good time to reflect on why this has been and have decided that something has to change with my project to really move it forward. I realised that there are two separate themes I am trying to address in my project, and that I think this is holding me back. The idea of trying to make AI more accessible and exploring how human activity might be aided by creativity in the future thanks to AI are two different topics and I need to narrow down in order to make real progress.

The work being carried out in the field of music, creativity and AI really interests me, and is an area I will continue to read about and research as a personal interest, but I have decided to focus on the educational aspect of my project moving forward with the masters final project. I feel that I got a bit stuck with incorporating music in my project, and although music might be one of the ways which I can engage people with learning about AI, I would like to explore other ways too. I plan to continue trying to include children in the process to add context to my project, and to actually start prototyping my ideas to have something physical to work with to get more useful user feedback.

One of the issues I think I will run into between now and the end of the masters is how to realise my project technically, so I need to start prototyping these aspects of my project now as well. To help with this I plan to better incorporate the content of the Fab Academy where possible and make use of the tutors and everyone within the Fab Lab.

## Design Dialogues 2.0

*Reviewers:Tomas Diez, Mariana Quintero, Ramón Sangüesa, Lucas Peña, Xavi Dominguez, Laura Cléries, Oscar Tomico, Liz Corbin, Jordi Montaner, Berta Lázaro*

The end of term presentation for the second term took the same format the the [Design Dialogues](https://mdef.gitlab.io/oliver.juggins/reflections/design-dialogues/) presentation in the first term and was another great chance to have conversations about my project and receive feedback. It was a really great experience to have this feedback in small groups and even one to one with a wide range of visitors and a nice opportunity to have in depth conversations about the directions the project could go in. This documentation is divided into the work I showed, a record of the conversations I had along with some reflections on how to improve my project and an intervention proposal which will take place in the third term.

### Work Presented

The work I presented was divided in the following sections and themes:

* Video showing context of the project.
* Overview of my area of intervention.
* The development and testing of my intervention which took the form of a workshop in a school.
* Evaluating the results of the workshop I carried out.
* A plan for the third term including proposed content of workshops.
* Mapping of my project in relation to the identified relevant themes in the Atlas of Weak Signals using keyword analysis.
* Demo of the activity I carried out in the workshop.

![]({{site.baseurl}}/stand-term-2.jpg)

<u>Video</u>

<br>
<iframe src="https://player.vimeo.com/video/331261904" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

Video showing context of project and some of the story behind the project.

<u>Area of Intervention</u>

*Mapping of the elements that contribute to the intervention*

![]({{site.baseurl}}/overview.jpg)

The mapping shows the different elements which together form my intervention including: the Atlas of Weak Signals, state of the art, development and testing (context), tools and methods, projection and speculation with their relation to one another where appropriate.

The ‘speculation’ element included a timeline which mapped out the influence of the weak signals I selected from now until the year 2050. A question I asked myself which guided this exercise was ‘How will a 13 year old learning about AI today benefit in the year 2050?’. This is something I will keep in my mind in the third term.

<u>Developing an Intervention</u>

*Documenting the instrumentation & application of The Puerta Project*

![]({{site.baseurl}}/developing-an-intervention.jpg)

![]({{site.baseurl}}/term-2-markups.jpg)

This section of my presentation showed how I had been developing my project through a workshop in a School in Badalona with Xavi Dominguez from the [Future Learning Unit](https://twitter.com/futurelearningu). It includes a summary of the workshop, the conditions and a lesson plan as well as hard copies of all the material I prepared for the workshop (version 1) and my process of marking them up for version 2). The material I prepared included: introductory presentation, lesson plan, teacher guide, student guide, extra activity and evaluation activity.

<u>Evaluating an Intervention</u>

*Documenting and reflecting on the results of an intervention*

![]({{site.baseurl}}/evaluating-an-intervention.jpg)

![]({{site.baseurl}}/evaluation.png)

From the workshop I recorded the results in the form of an evaluation activity I asked the students to carry out and then presented this in some diagrams and word clouds. This included quantitative and qualitative data from the answers the students provided which will inform the planning of future workshops. The word clouds for example picked out common words in the written answers from students so I was able to identify recurring themes that they wrote about. There are also some reflections about the activity as a whole and how some ideas for future workshops.

<u>Planning an Intervention</u>

*Mapping the application of The Puerta Project for Term 3*

![]({{site.baseurl}}/planning.jpg)

This was an exercise in planning the final term for my project mapping out the iterative process of developing the workshops. I would like to carry out a ‘series’ of two workshops in parallel: continuing the workshops with children in schools and co-designing workshops with teachers with the aim of being able to train teachers to carry out workshops in schools.

I also included the themes of the content of the workshops I would like to carry out relating to artificial intelligence and machine learning which are referenced in the weekly plan (colour coded).

<u>Atlas of Weak Signals</u>

*Situating The Puerta Project among ‘weak signals’*

![]({{site.baseurl}}/atlas.jpg)

I presented the work carried out in the Atlas of Weak Signals seminar in relation to my project. During the [development](https://mdef.gitlab.io/landing/term2/05-AtlasofWeakSignals-Development.html) module we researched the topics that were identified in the [definitions](https://mdef.gitlab.io/landing/term2/04-Atlas%20of%20Weak%20Signals%20-%20Definitions.html) and created a list of links. Through web-scraping these links keywords were then identified which I then presented as a series of word clouds for the relevant themes for my project. I identified keywords for the research carried out for my area of interest (AI literacy and education) and created a series of word clouds and created connections between the repeating words in each cloud. This forms the first iteration of visualising my project in the context of the ‘weak signals’ and is something I would like to develop in term 3.

<u>Demo</u>

![]({{site.baseurl}}/demo.jpg)

The activity I carried out with students in the workshop was presented on my laptop although there was never the opportunity to talk about it during the presentations. The workshop was created using an activity from [Machine Learning for Kids](https://machinelearningforkids.co.uk/) which is a machine learning extension on the visual programming language [Scratch](https://scratch.mit.edu/).

### Feedback

My notes on each of the conversation will serve as a record of the day and will include some reflections on how I can improve my project, activities and design actions for the third term.

<u>Tomas Diez</u>

The conversation with Tomas started around placing my project in the context of others who have been working in this field historically (computer science education for children) and to look into figures such as [Seymour Papert](http://www.papert.org/). We also talked about how I could create a narrative based around how a child could grow up with AI as a speculative exercise which could also feed into the workshops I am planning, imagining what a voice assistant in the year 2050 would be like.

A timeline ‘backcasting’ exercise could be a good way to start thinking about this speculation and imagining the infrastructure and complete story of how this object would be made in 30 years time, and key events that might happen between now and then. I would also like to use the skills and knowledge I have gained in the [Fab Academy](https://mdef.gitlab.io/oliver.juggins/fabacademy/) to prototype a speculative object based around this activity, or make something that could form part of the workshops I am planning.

<u>Mariana Quintero</u>

I had spoke a lot with Mariana through the second term and the presentation was a chance to continue the conversations we had been having about how to take the project forward. We talked about the vision of the project and what it aims to do, and how this could inform the business model of my project. This included discussing who will be carrying out the workshops I am preparing, if it only myself or something I could train teachers to do, and even design and develop with them. I am keen to test this idea, as I think it would be beneficial to both students and teachers if the activities are designed with their specific needs in mind.

Part of the narrative of my project is about the ubiquity of machine learning in society, so I think it would make sense to link a history or science class to machine learning and make it relevant for the students’ existing learning objectives and syllabus. As an outcome of the project I would then be able to have a series of workshops, products and packages that have been co-designed with teachers on a series of different topics.

As a result I am currently working with Xavi from the Future Learning Unit to try and arrange a workshop with teachers to get their feedback in the design of the workshops and hopefully for them to be able to carry them out independently as well. This will help test this idea of working with the teachers and test the model I had been talking to Mariana about.

<u>Ramón Sangüesa</u>

I had been looking forward to talking to Ramon about my project as although I had been working with artificial intelligence in my project for some time had not spoken to him about my project. He had lots of references for me to look at including the book [The Vestigial Heart](https://mitpress.mit.edu/books/vestigial-heart) by Carme Torras who is a Barcelona based researcher in robotics, artificial intelligence and ethics. Ramon also has experience in carrying out workshops around artificial intelligence and suggested incorporating physical activities into my workshops as a way to engage participants which is something I would really like to do. He is also keen to join the next workshop and I plan to talk to him a lot in the third term about my project to learn more about the types of activities he has done as workshops and to continue receiving feedback on the contents and delivery of the activities I am planning.

<u>Lucas Peña</u>

Lucas had similar feedback to Tomas in that I should frame my project in the wider picture of computer science education, adding the work of Jean Piaget to my reading list. He suggested that the more I am able to back up the development of the workshops with theoretical and scientific knowledge, the better as it will help me form a stronger argument. I think this will really help my project in understanding what my project is building on and how Piaget's theory of constructivism can help me understand better what I am actually doing.

For the workshops themselves he also talked about how I should continue the evaluation exercise and look for more ways of doing this and research tools for teachers and educators that might already exist. Something he mentioned which I will definitely incorporate into the next workshops is how I should always relate the learning about artificial intelligence to human intelligence. This could form a nice beginning / closing exercise to the workshop and could devise a way of measuring how students’ perception of intelligence changed in the course of the activity.

<u>Xavi Dominguez</u>

Xavi is part of the [Future Learning Unit](https://twitter.com/futurelearningu?lang=en) within the Fab Lab Barcelona and I have been developing my project with his guidance and carried out the workshop in the school with his help. The conversation was focused around what I aim to do next term and how to develop the project. We talked about the planning of the next workshop which could build on the first workshop by incorporating some physical computing. The workshop we carried out consisted of creating a Tourist Information guide in Scratch, so it could be nice as a follow up for students to prototype their own voice assistant and have something they can train and interact with.

We also talked about how we could plan an activity for teachers and am currently planning this with Xavi to take place in early-mid May. I then need to diagrammatize this process of teaching the teachers and how I can get results and the learning outcomes from these activities and use the teachers’ knowledge to plan activities and put into practice what I was discussing with Mariana.

<u>Laura Cléries</u>

Laura suggested that I relate the content of all of my workshops to ethics, and that this should be a stronger feature in my project. I think this is a good idea and something which can be included in the reflective elements of the classes as ethics relates to multiple facets of artificial intelligence and machine learning. Like Lucas, Laura recommended looking into more methods for evaluating student learning and how to evaluate non academic knowledge, which could include engagement in ethical debates around artificial intelligence.

We also talked about how I could communicate my project and recommended looking at [Project H](http://www.projecthdesign.org/) in how they have setup their platform and they display and talk about their project. Laura also suggested thinking about additional literature I could provide which could be a novel formed from my own reflections from the workshops. I think writing a novel in the third term might be ambitious but a short story which incorporates the speculative exercise I was discussing with Tomas is something I would like to do.

<u>Oscar Tomico</u>

My conversation with Oscar was focused around how I should think about how much project will impact the lives of the children I aim to teach artificial intelligence to, and the consequences of opening up the ‘black box’ to them. I think this ties into the speculative exercise I have been referring to in imagining how a child grows up with AI, and how their knowledge about AI could form their relationship with this technology. I could start this process by being critical in the recording and documenting of the workshops I carry out and encourage reflection on the students’ part on getting them to think about how learning about AI might help and empower them.

### Intervention Proposal

The intervention I am proposing for the third term will continue the work I carried out in the second term in the development and execution of workshops in schools. These workshops aimed at students will be accompanied by workshops and sessions for teachers including co-design activities which will inform the workshops for students. They might reach a point of development where teachers are able to carry out these workshops independently. As a result, workshops with school children and teachers and the documentation, evaluation and reflection on these workshops will be the primary activity I will carry out relating to the design studio next term.

As well as workshops, from the conversations I had during the final presentations, there other additional activities emerged and actions that I plan to do which will also form part of and help the development of my intervention (including when I plan to do them). These include:

* Research into theoretical / scientific background of learning using the work of Piaget, Papert. This will frame my project as part of the contribution to computer science education and view the inclusion of artificial intelligence and machine learning as part of this. *(Week 1)*
* A series of speculative design exercises which will take the form of a short story and object (voice assistant for the year of 2050). *(Weeks 2-4)*
* Development of ‘AI curriculum’ which goes with the series of activities. Inclusion of teachers in this process is necessary. *(Weeks 3-6)*
* Continue working on linking Atlas of Weak Signals to my project and think about different ways to represent this, in three dimensions and virtually. *(Weeks 3-5)*
* Continue using data from workshops employing the technical skills gained in Atlas of Weak Signals to analyse the evaluation from the workshops I do to inform future activity planning. *(Weeks 2-6)*

Note: The dates of workshops are yet to be defined but will mostly likely be between weeks 2-5. The plan I created for the presentation (Planning an Intervention) will also be used as a guide for the planning of the workshops on a weekly basis.

All of this material will then inform the creation of a ‘package’ which includes research, a series of workshops for teachers and students (including guides etc) and a platform which will serve as a resource for both educators and children to learn about and engage with artificial intelligence.
