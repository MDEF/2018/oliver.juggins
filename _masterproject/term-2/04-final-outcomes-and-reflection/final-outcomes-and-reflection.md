---
title: Final Project Outcomes & Reflection
period: # 05 October 2018 - 20 December 2018
date: 2019-01-24 12:00:00
term: 2
published: true
---
*Faculty: Tomas Diez, Oscar Tomico and Mariana Quintero*

This page documents the final outcomes of the project and all of the support material and images that communicate the project. Following this material the content shown in the exhibition is also shown.

This page provides a record of where the project has reached during the masters. A plan for the future is to develop the online presence of the project which can be found here:

* [thepuertaproject.eu](http://www.thepuertaproject.eu/)
* @the_puerta_project

<!-- Mention santi & xavi here little description of project

I would like to give a special thanks to Santi Fuentemilla and Xavier Domínguez of the Fab Lab Barcelona’s Future Learning Unit for giving me the opportunity to work with them to
explore how artificial intelligence can be introduced into the learning environment. I also would like to thank all of the open source projects and platforms that I have been able to use to develop the project, such as [<u>Scratch</u>](https://scratch.mit.edu/)

A very special thank you to Santi Fuentemilla and Xavier Domínguez of the Fab Lab
Barcelona’s Future Learning Unit for giving me the opportunity to work with them to
explore how artificial intelligence can be introduced into the learning environment.
Thank you to all of the open source projects and platforms that I have been able to
use to develop the project. Scratch, Cognimates, Machine Learning for Kids, and the
Wekinator are just some of these.
Finally thank you to all of the students and teachers who have participated in the project
so far. Their ideas, thoughts, speculations and reflections are interspersed throughout
this piece of writing to record the themes and ideas that arose in our conversations and
activities surrounding artificial intelligence, learning, teaching and children.

The Project has been developed in collaboration with the Fab
Lab Barcelona’s Future Learning Unit (Santi Fuentemilla &
Xavier Domínguez) as part of the first edition of the Master in
Design for Emergent Futures (2018-2019). -->

## Written Article

<u>Abstract & Written Article Description</u>

 *The Puerta Project provides everything required to bring machine learning and artificial intelligence principles to the learning environment. The project applies design-based research methodologies to create bespoke, fun and engaging STEAM activities for children between the ages of 8-15, framing itself within the growing field of ‘AI literacy’.The realisation of the project consists of a series of workshops that not only make children aware of the presence of machine learning in their lives but present it as a tool to further and extend their practice, creativity and curiosity. The ultimate aim is that with this knowledge, children that learn about artificial intelligence won’t think of a voice assistant as a black box with magical powers but something they could build themselves and even adapt for their own needs.*

 *The methodology that has been developed has been influenced by learning theories that have shaped the digital literacy space over the last half-century and incorporates the views and ideas of AI experts, educators and children. The project is open-source and uses freely available platforms and affordable microcontrollers to promote its accessibility.*

 *This piece of writing includes the main motivations behind the project, documented design actions, a series of speculative design exercises to project the project in the future and a proposed sustainable business model. The final chapter includes a series of reflections about the masters as a whole and what I have taken away from the whole experience.*

![]({{site.baseurl}}/thesis-gif.gif)

Click to download and view the written article in full: [<u>The Puerta Project: A methodology to bring machine learning to the classroom environment</u>]({{site.baseurl}}/oliver-juggins-puerta-project-written-article.pdf)

## Project Images

The supporting images for the project cover the different elements of the intervention. This includes pictures taken during the workshop themselves of students and prototypes that they have made, images of the project manual which contains the methodology and a machine learning starter kit which is something that is planned for the project in the near future.

{% figure caption: "*Students during workshop in discussion.*" %}
![]({{site.baseurl}}/oliver-juggins-puerta-project-image-1.jpg)
{% endfigure %}

{% figure caption: "*Helping students with coding in App Inventor.*" %}
![]({{site.baseurl}}/oliver-juggins-puerta-project-image-2.jpg)
{% endfigure %}

{% figure caption: "*Student programming the machine learning communication with the prototype via WIFI.*" %}
![]({{site.baseurl}}/oliver-juggins-puerta-project-image-3.jpg)
{% endfigure %}

{% figure caption: "*Final prototype of project in context: the Fab Lab BCN.*" %}
![]({{site.baseurl}}/oliver-juggins-puerta-project-image-9.jpg)
{% endfigure %}

{% figure caption: "*Image showing the key technologies and skills of the project: machine learning, programming and digital fabrication.*" %}
![]({{site.baseurl}}/oliver-juggins-puerta-project-image-10.jpg)
{% endfigure %}

{% figure caption: "*Image showing proposed machine learning starter-kit including BBC Micro:Bit, servos and instructional booklet.*" %}
![]({{site.baseurl}}/oliver-juggins-puerta-project-image-5.jpg)
{% endfigure %}

{% figure caption: "*The printed project manual in book format.*" %}
![]({{site.baseurl}}/oliver-juggins-puerta-project-image-8.jpg)
{% endfigure %}

{% figure caption: "*Glimpse of the project manual.*" %}
![]({{site.baseurl}}/oliver-juggins-puerta-project-image-6.jpg)
{% endfigure %}

## Project Support

The project support consists of a methodology which has been realised in the form of a manual and a presentation prototype which will feature in the exhibition.

<u>The Puerta Project Manual</u>

![]({{site.baseurl}}/oliver-juggins-puerta-project-image-4.jpg)

<!-- ![]({{site.baseurl}}/manual-gif.gif) -->

This is a manual aimed at teachers who are interested in bringing artificial intelligence and machine learning to the classroom. It outlines background information about the project, machine learning and sample activities that have been developed as part of the project (beginner, intermediate and advanced levels). Here are some of the sample activities:

![]({{site.baseurl}}/worksheets.png)

<!-- images of worksheet / reflections -->

Click to download and view the project manual in full: [<u>The Puerta Project: A guide to bring machine learning to the classroom</u>]({{site.baseurl}}/oliver-juggins-puerta-project-manual-MDEF.pdf)

<u>Machine Learning Starter Activity</u>

This activity is delivered in the form of a presentation which has been developed as an introduction to machine learning, made for total beginners with the audience of the exhibition in mind. It has been developed through testing the workshops and activities in the design actions that I have carried out so far during the project.

The introductory section includes an interactive element through inviting participants to write their definition of intelligence and to publish it to a [<u>website</u>](https://intelligence-is.netlify.com/). This was created with web development and support of Ilja Panic. The aim of this exercise is to encourage participants to not separate ‘human’ and ‘artificial’ intelligences and to think critically about the notion of intelligence. The idea is that this piece becomes a long-term project where all of the responses of people who participate in the project and take part in a workshop are recorded.

Click [<u>here</u>](https://intelligence-is.netlify.com/) or visit [<u>https://intelligence-is.netlify.com/</u>](https://intelligence-is.netlify.com/) to record you own definition of intelligence.

![]({{site.baseurl}}/intelligence-is.png)

A step-by-step guide then is provided for participants to follow at their own pace (they will have access to the presentation) where they build their own machine learning model (text sentiment analysis). The model they will create is simple, with only two labels (happy and sad). This is then transferred to Scratch using the Cognimates platform which they then have to create a simple sketch to make a character happy or sad.

The BBC Micro:Bit is then introduced to add a physical computing element to the activity. The programming is very basic, but adds something in the physical world which has been an important development for the project.

A reflective exercise and discussion then follows, where I ask if their model responded in the way they envisaged, and if not then why this might be. I then try to make the connection to this in real world setting to introduce the concept of AI bias.

To close, participants are invited to carry out an evaluation form which will then inform future workshops through any feedback and add to the research I have been carrying out about the future of learning and teaching in the context of artificial intelligence.

<br>
<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vR2emBL2YX1B1IdJQV6Yckz7iX8S_jRF1O1LDjXGLgh79si0gA8XSuh0ceP2kx9moD9DfzKg3ChWsmG/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="360" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## Videos

I created two videos to communicate the project. This first provides all of the context, motivations, aims and objectives and gives the full story about why The Puerta Project exists and the societal issues it addresses. Icons and animations are used with narration to tell this story. (3mins 45s)
<br>
<br>
<iframe src="https://player.vimeo.com/video/331261904" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>


The second video provides more detail about the primary intervention of the project: workshops. Like the manual, the video is aimed at teachers and educators who might be interested in applying machine learning to the classroom. It provides a glimpse of some of the activities that happen and the skills students can expect to develop using footage from workshops carried out in the Fab Lab for the project's development. (2min 55s)
<br>
<br>
<iframe src="https://player.vimeo.com/video/344153238" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

## Exhibition material (learning (with) machines)

The material shown in the final presentation is split into various parts which is outlined below. The exhibition workshop with Daniel Charny of [<u>From Now On</u>](http://www.fromnowon.co.uk/) helped me to decide what should be included and imagine the experience of a visitor. The exhibition is entitled 'learning (with) machines' as a way to try and present and frame the whole project with the idea of *extended* intelligence rather than artificial intelligence and to view the human-machine relationship as collaborative.

<u>Project Posters & Title</u>

The main posters give some basic knowledge about machine learning and cover the methodology and some documentation of an intervention. Hover over the image to read in more detail. The centrepiece is the element 'intelligence is__' to invite people to interact with the installation.

In this section some prototypes and outcomes of the workshops that studetns have taken part in are displayed.

<img
 class="zoomable_image" src="{{site.baseurl}}/exhibition-material-small.jpg" data-zoom-image="{{site.baseurl}}/exhibition-material.jpg"
 />

 <script>
 $(".zoomable_image").elevateZoom({
   zoomType				: "inner",
   cursor: "crosshair"
 });
 </script>

<u>Project Flyer</u>

Includes the project's mission and the guiding practices and principles which feature in the manual.

![]({{site.baseurl}}/project-flyer.png)

<u>Children, Learning, Teaching & AI</u>

A series of speculations and reflections from teachers and children that have taken part in the intervention thus far are to give a glimpse of the kinds of ideas and discussions that have taken part in workshops.

![]({{site.baseurl}}/reflections.jpg)

<u>The Puerta Project: 1950 - 2050</u>

An interactive timeline places the Project in the context of developments in artificial intelligence, machine learning and relevant pedagogical theories. The project is also imagined from the present day until the year 2050, investigating how teaching, artificial intelligence and learning might change over the next 30 years. The relevant ‘Weak Signals’ for the project (as seen on the fabric soffit of the exhibition) are also mapped onto the timeline.

![]({{site.baseurl}}/project-timeline.jpg)

<!-- short thing about general takeaways. Exhibition. Workshops. Put in website link here. Plan for the future to build online platform and social media presence -->
