"I am a designer with an architectural background who graduated from the University of Cambridge (2014). I have worked at a range of architecture, interior design / product design studios in the UK (Lynch Architects, Universal Design Studio, Studiomama) and more recently in Colombia (Tu Taller Design) where I assisted in the installation and production of the Colombian pavilion for the London Design Biennale (2018).

My current areas of interest include participatory design practices, possible future(s) of the architect / designer, the future of housing / living and technological literacy in society."

![](assets/autonmycubepaglen.jpg)
