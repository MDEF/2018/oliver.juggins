---
title: Navigating the Uncertainity
period: 29 October - 4 November 2018
date: 2018-11-04 12:00:00
term: 1
published: true
---
*Tutors: Tomas Diez, Oscar Tomico and Mariana Quintero*

Navigating the Uncertainty was a week of discussions and seminars from invited speakers (Jose Luis de Vicente and Pau Alsina) and tutors which covered wide ranging topics such as climate change, moral responsibility of the designer, systems theory, blockchain and more. The discussions and readings opened up many new ideas, concepts and philosophies which I initially found difficult to process but ultimately developed the theme I want to explore as part of my project significantly. The documentation this week consists of the route I have taken to get to my current thinking from the different activities during the course.

![]({{site.baseurl}}/break10.png)

### Uncertain Times

The first talk of the week from Jose Luis de Vicente focused on climate change and the subsequent dangers humanity faces in the coming decades if we don't properly address global warming. The talk made me think about the other problems society faces and what can be done to begin trying to solve them. The three main threats to humanity as defined by Yuval Harari are nuclear war, climate change and technological disruption. These are not simple problems to solve, but complicated ones which are embedded in complex political, ecological and technological systems. How can long term thinking be implemented in a system where short term gains are prioritised? How can trust be built between nations to tackle these issues on a global scale, building global identities? How did we get here and what happens next?

These are just some of the questions that came to mind following the initial discussion. There are no easy answers but there is some explanation to why these issues are so hard to change. Donella Meadows in her essay [Leverage Points: Places to Intervene in a System](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/) proposes a scale twelve leverage points to intervene in a given systems with corresponding levels of effectiveness, observing that small shifts have the potential for dramatic changes. These shifts that could cause this dramatic change are often counterintuitive to the problem addressed and therefore hard to implement and make any changes. Furthermore, the leverage point Meadows identifies as the least effective (changing of parameters) are the ones most often implemented, describing how we often focus on the completely the wrong variables when trying to implement change.

Another issue relating to leverage points is even if one is identified and implemented what’s to say if it will make a difference? Using climate change as an example, in order for large changes to happen multiple changes need to be implemented and happen simultaneously all over the world which calls for a type of coordination on a global scale which does not currently exist. These are not technical problems that can be solved by individual actions but ones that can only be addressed through a radical change in society.

A radical change in society in the future is proposed in the installation [After Abundance](http://www.londondesignbiennale.com/countries/austria/2018 ) shown at the London Design Biennale as part of the Austrian pavilion offering a view of a society which has attempted to deal with the realities of climate change through human intervention and adaptation.

{% figure caption: "*Photo Credit: Ed Reeve*" %}
![]({{site.baseurl}}/austria.jpg)
{% endfigure %}

<u>References</u>

[Anthropocene Working Group](https://theanthropocene.org/topics/anthropocene-working-group/)

[The Edge of Humanity: A Conversation with Yuval Noah Harari](https://samharris.org/podcasts/138-edge-humanity/)

[Long Now Foundation](http://longnow.org/)

[Austrian Pavilion London Design Biennale](http://www.londondesignbiennale.com/countries/austria/2018 )

![]({{site.baseurl}}/break10.png)

### Inventing the Future

Exploring the idea of the radical change necessary in society to combat climate change, I searched for visions of a future which provide a positive outlook and one I would want to be a part of. I also carried out this exercise as a way of imagining a future context for which to design. In [*Inventing the Future: Postcapitalism and a World Without Work*](https://www.versobooks.com/books/2315-inventing-the-future ) authors Nick Srnicek and Alex Williams provide a vision for a future with an alternative political system, proposing a manifesto in which humanity is free from work. This idea of freeing people from work has a long history, but the authors argue the threat of automation and the subsequent breakdown of stable jobs in developed countries and rising populations has given a renewed significance to the issue. They put forward some compelling arguments for why society should shift to a world where work is no longer the primary activity of someone’s life.

Srnicek and Williams’ vision views changes brought on by technological disruption as an opportunity rather than a danger. They argue the dramatic change in political system would allow people to take back control of their lives, time and communities and provide clear action for the left to take for this change to take place.

There are four demands that the authors have which outline what this post-work world would mean in practice. There are clear links between these four demands and would have to be addressed as a whole, which I will briefly outline.

The first demand is full automation which simply means and getting rid of jobs using the latest technological developments, meaning machines would release people from carrying out the work in society that produces all goods and services.

The second demand is a reduction in the working week, which would begin to be implemented as automation starts replacing more jobs. This would not only be desirable through providing more leisure time, but would also help combat climate change. There have been [studies](https://www.independent.co.uk/environment/bank-holiday-three-day-weekends-climate-change-environment-working-hours-a7215421.html) which show a four day work week would have positive implications for climate change. A huge reduction in resource consumption due to there being a day less of commuting and offices and factories running explains this benefit.

The third demand is a universal basic income which would allow people to choose how to spend their time and provide economic stability for an individual if their work is replaced. Working would be something that beomes voluntary, and would empower people when looking for work as the reliance on income from labour would be eliminated. Srnicek and Williams outline three factors necessary for this universal basic income: that it must be provided to everyone unconditionally, is enough income to live on and must supplement the welfare state, not replace it.

Although the idea of universal basic income is by no means new, it is becoming a topic more widely discussed, especially given that US presidential candidate [Andrew Yang](https://www.yang2020.com/) is a strong believer which he discusses in his new book [*The War on Normal People: The Truth About America's Disappearing Jobs and Why Universal Basic Income Is Our Future*](https://www.yang2020.com/buy-the-book/).

The final demand outlined is a reduction in work ethic, which is more a cultural issue to combat rather than a political one. Work and having a job is such an integral part of people’s identity and the idea of losing that part of our lives and replacing it would be a great challenge. What would a society look like that is not focused on having a job would be a key question to consider.

If universal unemployment is a reality facing humanity, then as a society discussions need to start which imagine futures for this scenario engaging with the wider public. A strategy will also be necessary which balances long and short term thinking as automation starts replacing jobs and putting everyone eventually out of work as the book suggests. How to give people meaning and how people will spend their time are critical considerations. Time and identity are two issues I am interested in exploring as society’s idea of how people feel valuable and what they fill their days with will have to completely change.

Jay Griffiths, author of *A Sideways Look at Time* explores different conceptions of time around the world, which I thought could relate to thinking about time in the future and how it might change. Griffiths describes how the conception of time has changed throughout history and its embedded political nature through the sense of time around the world during changing due to the empire. She discussed different ways of telling the time and some unconventional time devices such as birdsong which members of the Kaluli tribe in Papua New Guinea use as a signal for light levels and a clock which is made from flowers by Swedish botanist and physicist Carl Linnaeus. If people have more time for leisure and other activities through automation would this change how we think about time, and what would a clock in a post-work world look like?

<u>References</u>

[Inventing the Future: Postcapitalism and a World Without](https://www.versobooks.com/books/2315-inventing-the-future) by Nick Srnicek and Alex Williams

[The War on Normal People: The Truth About America's Disappearing Jobs and Why Universal Basic Income Is Our Future](https://www.yang2020.com/buy-the-book/) by Andrew Yang

[Radiolab - Time](https://www.wnycstudios.org/story/91584-time), May 2007

[A Sideways look at Time](A Sideways Look at Time) by Jay Griffiths

![]({{site.baseurl}}/break10.png)

### Revised Interests

During Navigated the Uncertainty the amount of new ideas from seminars and readings developed my thinking a great amount which I have consolidated below:

![]({{site.baseurl}}/interests.png)

![]({{site.baseurl}}/break10.png)

### Participatory Design

I would like to briefly discuss my thoughts on participatory design as it will inform how I implement it into my own work and is something I have reflected on this week. The reflections were a result of Pau Alsina’s talk which discussed many topics but one that resonated with me was the idea of the design world being in a ‘black box’ and how the discipline could be opened up and become more inclusive.

Carlo Ratti’s [Open Source Architecture](http://senseable.mit.edu/osarc/) has informed my current thinking on the subject greatly. The book discusses how architecture could apply open source practices to the design of buildings, citing the success of Linux as a hybrid system involving content generated by users which is then moderated by a team of developers.

Open Source Architecture proposes that the role of an architect could to evolve into more of a facilitator than a designer, enabling users to have a greater creative capacity. I am interested in how this could work in practice, not only limited to architecture but other design disciplines as I think having an open framework which the designer has formulated leaving opportunities for input from other sources and users could lead to outcomes not otherwise possible.

Another aspect of participatory design that I feel is important and could be relevant to my work is the idea of memory and experience. Through doing something yourself better understanding and engagement is possible, especially if that something is in an interesting setting or something new. Experiencing something first-hand gives you something to refer back to and provides a more memorable and personal connection.

A final comment on participatory design is how technology can allow for new opportunities for communication between the designer and user. I would like to explore how interfaces can allow the design process to become more of a dialogue and how this relationship can impact on the design process.

<u>References</u>

[Open Source Architecture](http://senseable.mit.edu/osarc/) by Carlo Ratti

[Spatial Agency](http://www.spatialagency.net/#)

![]({{site.baseurl}}/break10.png)

### Case studies

As an exercise I have carried out a series of case studies to generate ideas relating to my interests from the diagram of my revised interests. The case studies and references are diverse, ranging from products currently in the market to artists working with technology and speculative design studios and projects.

<u>Making Sense</u>

Making Sense is a project which ran between 2015-2017 and is relates to my interest in using participatory design as a means of democratization of new technologies. The project enables communities to have a direct impact on their environment whilst learning new skills through making their own sensing tools to measure environmental conditions such as air quality and noise pollution. Data gathered from sound sensors for a project in [Placa del Sol](http://making-sense.eu/campaigns/placa-del-sol/), Barcelona, provided hard evidence of sound levels which informed policy at a local level. The project is a great example of how technology can bring a community together to form positive change, empowering individuals initially through the process of making the sensors and then through seeing their work have an impact on the wider community. The project is open source and you can download the toolkit [here](http://making-sense.eu/publication_categories/toolkit/).

![]({{site.baseurl}}/making.jpg)

<http://making-sense.eu/>

<u>Bare Conductive</u>

Bare conductive is a company that produces printed electronic items that allows people to start playing with and integrating electronics into their own environment right out of the box. They have developed a range of kits for prototyping electronics including the [Touch Board Starter Kit](https://www.bareconductive.com/shop/touch-board-starter-kit/) and a conductive paint which allows you to make unconventional circuits encouraging novel ways of interacting with a circuit and electricity.

![]({{site.baseurl}}/bare.jpg)

<https://www.bareconductive.com/>

<u>Kano</u>

Kano is a computer kit aimed at children which opens up the world of coding and encourages computational thinking. It encapsulates the learning by doing methodology as you are required to build your own computer to being with, first learning about how the different physical elements of a computer come together before learning about how computers operate through projects with the software to start programming in different languages such as Python, JavaScript and more.

![]({{site.baseurl}}/kano.jpg)

<https://kano.me/>

<u>Art as a Tool for Engagement</u>

"The job of the artist is not to be a technical innovator but a social innovator."

*Kyle Macdonal, IaaC public lecture, 2018*

I have used the work of three different artists with different approaches as references which will be useful for my own work as I am interested in how art can be used to engage with difficult issues and subjects of our time.

<u>Yuri Suzuki</u>

Yuri Suzuki works between art, design and music and has developed different participatory projects involving technology, music and interaction. He is interested in the relationship between sound and people and his project [*Looks Like Music*](http://yurisuzuki.com/artist/looks-like-music) is an example of exploring this relationship. Miniature robots follow tracks drawn by marker pens on paper which the public are invited to participate in through adding their own colours to the installation to create sounds. This results in a large scale artwork and an accompanying sound piece.

![]({{site.baseurl}}/yuri.jpg)

<http://yurisuzuki.com/>

<u>Trevor Paglen</u>

*You can imagine that you are someone in the future looking back in the past and it causes you to see the present through different eyes and perhaps allows you to notice things that you might otherwise notice. It allows you to see the present in a different way.* - Trevor Paglen, 2017

Artists working with technology who use their work as a tool for engagement about contemporary issues are of particular interest to me and [Trevor Paglen](http://www.paglen.com/) is a great example. His work encompasses sculpture, journalism, image-making and he explores  themes such as data collection and surveillance. He is also concerned with imagining alternative futures and provoking critical reflection on different contemporary technological issues. An example of this is his piece Autonomy Cube which is an installation which looks at ideals of anonymity, liberty and privacy using the museum and gallery as a setting for this commentary. The work creates an open Wi-Fi hotspot which anyone can join, but is not connected to the internet in the typical fashion, instead routing the traffic over the Tor network which is designed to anonymize data.

![]({{site.baseurl}}/cube.png)

<http://www.paglen.com/>

<u>Theaster Gates</u>

The aspect of the work of Theaster Gates that interests me is his interest relating to the transformation of communities through participatory practices. Gates started out as a potter but his work involves, painting, sculpture and has expanded into a social practice through renovating multiple abandoned buildings converting them into vibrant [cultural hubs](https://www.smithsonianmag.com/innovation/theaster-gates-ingenuity-awards-chicago-180957203/). He founded the [Rebuild Foundation](https://rebuild-foundation.org/) which supports artists and aims to strengthen communities through free arts events, the creation of affordable housing, studio and live-work space and more. The foundation is based in Chicago, where Gates is originally from and they 'leverage the power and potential of communities, buildings, and objects that others have written off'.

{% figure caption: "*Black Cinema House, Chicago*" %}
![]({{site.baseurl}}/gates2.jpg)
{% endfigure %}

[Rebuild Foundation](https://rebuild-foundation.org/)

<u>Additional Artists of interest</u>

* [Kyle Kyle McDonald](http://www.kylemcdonald.net/)
* [Zach Lieberman](http://thesystemis.com/)
* [Chris Jordan](http://www.chrisjordan.com/gallery/rtn/#light-bulbs) (running the Numbers series)
* [Muf Architecture/Art](http://muf.co.uk/)
* [Public Works](https://www.publicworksgroup.net/)

<u>Speculative Design</u>

Imagining future scenarios brought about by new technologies is something I would like to do in my work. The idea of speculating is a key part of forming a vision for a project as I learnt about last week during Hybrid Profiles. The work which designers carry out when working in this way often takes the form of immersive, experience-oriented installations which I find a really interesting medium and one I would like to explore.

<u>Superflux</u>

The work of [Superflux](http://superflux.in/)  has been a very important reference point for developing my thinking this week. In their work they ‘create worlds, stories, and tools that provoke and inspire us to engage with the precarity of our rapidly changing world.’ They view the planning of futures as something that should be inclusive with a focus on designing experiences hoping to show how one can adapt to changes in society and flourish in the world today.

They explored the theme of automation and the future of work which I researched this week in their project [*How Will We Work?*](http://superflux.in/index.php/work/how-will-we-work/#). The exhibition starts a dialogue about the different issues relating to the future of work bringing together the responses from a number of artists and designers. The show acted as the start of an experimental new research department based at the University of Applied Arts in Vienna, FORW:’ARD (Future of Rest and Work: Angewandte Research Department) which continues research into the fields of rest and work.

![]({{site.baseurl}}/work.jpg)

<u>Face Values</u>

[Face Values](http://www.londondesignbiennale.com/countries/usa/2018) is an installation that was shown at London Design Biennale 2018 as part of the USA pavilion. The public was invited to participate in an interactive performance using their facial expressions to control sound and graphic displays. The project interests me as it used technologies typically used in security, surveillance and behavioural profiling in an alternative way, communicating to the participants what these technologies are capable of through something interactive and playful.

{% figure caption: "*Photo Credit: Ed Reeve*" %}
![]({{site.baseurl}}/face.jpg)
{% endfigure %}

<u>Further Projects of Interest</u>

* [Ottonie von Roeder’s project Post-Labouratory](http://www.ottonieroeder.de/post-labouratory/), shown at the [Istanbul Design Biennale](https://www.domusweb.it/en/events/istanbul-design-biennial/2018/09/18/a-laboratory-to-imagine-a-world-without-work.html) explores a post-work world which invited participants to build a low-tech robot to replace work activities.
* [Domestic Data Streamers](https://domesticstreamers.com/)
* [Platoniq](http://platoniq.net/en/)
* [Llum Bcn](https://www-lameva.barcelona.cat/santaeulalia/en/llumbcn)
* [Nuit Blanche](https://nbto.com/)
* [IF Design Studio](https://projectsbyif.com/)
* [EPFL+ECAL Lab](http://www.epfl-ecal-lab.ch/)

![]({{site.baseurl}}/break10.png)

### Revised Vision

The case studies and activities carried out during Navigating the uncertainty has altered and evolved my vision. The idea of designing and creating experiences to promote critical engagement about a particular issue relating to emerging technologies is the main theme I would like to pursue. I am also really interested in having a strong relation to context in my project and the references I have used relating to community engagement has helped develop this thinking, notably [*Making Sense*](http://making-sense.eu/). As a result I think the challenge will be linking these two themes, exploring how communities can be built around gaining understanding about new technologies.

![]({{site.baseurl}}/break10.png)

### Reading list

A list put together for further reading relating to my areas of interest:

* Developing Citizen Designers by Elizabeth Resnick
* Taking [A]part: The Politics and Aesthetics of Participation in Experience by John McCarthy and Peter Wright
* The Shallows by Nicholas G. Carr
* The Fourth Industrial Revolution by Klaus Schwab
* The Future of the Professions: How Technology Will Transform the Work of Human Experts by Richard Susskind & Daniel Susskind
* How to Thrive in the Next Economy: Designing Tomorrow's World Today by John Thackara
* Inadequate Equilibria: Where and How Civilizations Get Stuck by Eliezer Yudkowsky
* You Are Not a Gadget by Jaron Lanier
* Modern Man in the Making by Otto Neurath
* The Global City by Saskia Sassen
* 21 Lessons for the 21st Century by Yuval Noah Harari


<!-- {% figure caption: "*Markdown* caption" %}
![]({{site.baseurl}}/transfolab.jpeg)
{% endfigure %} -->
