---
title: An Introduction to Futures
period: 3-9 December 2018
date: 2018-12-09 12:00:00
term: 1
published: true
---
*Tutor: Elisabet Roselló Román*

<p>
<font size="5">It's a long held truism that people use the future to indirectly talk about the present - Robin Hanson, 2018</font>
</p>

An Introduction to Futures opened up the world of future thinking, an essential topic to cover for any masters with 'emergent futures' in its title. There was a lot of theoretical content during the week but this was balanced with some practical methods and some exercises with some future scenario building exercises which can be applied to developing my own project. My recent ‘future thinking’ has been largely shaped by the writing of [<u>Kevin Kelly</u>](https://kk.org/), one of the founding members of Wired Magazine who has written on future-oriented topics for the past couple of decades and someone I think has an interesting outlook on the way new technologies are developed and deployed in society. Since starting the masters this thinking has developed greatly through the various references I have been looking at relating to my research into artificial intelligence and machine learning. As a result the week was a great opportunity to add to my limited knowledge about all things future-oriented.

![]({{site.baseurl}}/break10.png)

### Future Thinking

The timing of An Introductions to Futures felt fitting following [<u>Engaging Narratives</u>](https://mdef.gitlab.io/oliver.juggins/reflections/engaging-narratives/) as stories have been a large part of how notions of the future have been formed and serve as a reference point for many people when thinking of the future. There are repeating themes and ideas that appear in how the future is conceived in film and literature which range from the rise of the robots to flying cars and a world in which people live under control and surveillance from the state. Science fiction often tries to predict the future, often incorrectly which is a theme [<u>Ted Kupper</u>](https://twitter.com/tedkupper) and [<u>Jon Perry</u>](http://jonperry.com/) discuss in their podcast [<u>Review The Future</u>](http://reviewthefuture.com/?p=518). They argue that science fiction should perhaps adopt a more rigorous approach in how it constructs its narratives and avoid some of the typical clichés that science fiction writers turn to. Some of theses clichés they discuss include the lone inventor, the idea of human specialness and societal regression.

Brian Merchant, an editor at VICE also discusses the theme of science fiction in future thinking in his recent [<u>article</u>](https://medium.com/s/thenewnew/nike-and-boeing-are-paying-sci-fi-writers-to-predict-their-futures-fdc4b6165fa4). He describes how corporations such as Nike and Boeing are turning to science fiction writers for their own future forecasting and the creation of speculative worlds. The theme of worldbuilding was touched on a lot throughout the week and part of the technique involves studying the past and change, and how change occurs and then extrapolate to imagine multiple futures. These multiple futures can be extrapolated at different scales in order to gain different levels of insight as to how a particular future scenario might play out.

![]({{site.baseurl}}/future.png)

![]({{site.baseurl}}/break10.png)

### Future Vision, Utopias & Architects

Building possible futures has close ties to the notion of utopia and is a topic architects have long been concerned with. The architect as a practitioner is always projecting an idea about how people in the future should behave and occupy the world. The design decisions they make, whether they are aware of it or not force people to behave in certain ways and inhabit space through how they live in the design of housing, how they work through how offices are configured and even how we move through the city zooming out to the scale of a masterplan. The new materials and constructional methods that allowed architects to explore new ways of building in the modernist era of architecture gave way to a whole new set of ideas for how people should occupy buildings and live in this new industrialized world.

The notion that people should live in large homogenous blocks is an idea that dissipated throughout Europe and was interpreted around the world in different ways. This is thanks to architects such as Le Corbusier and other members of the Team X collective, who had strong views on how this world should be designed and developed. Buildings of this period and the ideas of architects such as Corbusier were hugely influential in their bold vision of the future, but are also often criticized for the way in which they don’t allow for individuality and treat all members of society in the same way, dictating a rigid a strict way of living. Not only this but the fact that this architecture doesn't necessarily take into account place and context where the notion of place is often forgotten.

{% figure caption: "*Le Corbusier’s unbuilt vision for Paris, Ville Radieuse, 1930*" %}
![]({{site.baseurl}}/corb.jpg)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Projection not Prediction

<p>
<font size="5">You can imagine that you are somebody in the future looking back in the past and it causes you to see the present through different eyes and perhaps allows you to notice things that you might not otherwise notice. It allows you to see the present in a different way. - Trevor Paglen, 2017</font>
</p>

The week provided a number of tools and techniques to help think about and form ideas about the future and what the field of ‘future studies’ encompasses in general. This section will act as a brief summary about some key ideas and concepts regarding future projection and will serve as a reference for thinking about these issues with regard to my own project as the masters progresses.

One key principle is the idea that predictions are not made about the future or future scenarios, rather projections and forecasts are made. It is also critical to consider possible *futures* and describe the future in a pluralistic way, as to make human choice relevant in conversations about what future we are imagining and projecting. Without this choice, then how can a conversation happen at all? Narratives can also be made to project futures, and should consider what is known about the current situation about a particular theme or topic. Through this analysis what is not known should be made evident and therefore provide insight into what should be learnt about them in order to imagine how change can be made.

These narratives should be consistent and plausible in describing in how a future occurrence comes to be and what its effects will be. It should also question the status quo and is an opportunity to start a conversation about a particular issue and engage an audience to think about an issue and consider the implications and consequences of decisions that are being made in the present. This idea of starting a conversation is the one I find most interesting as it allows people to engage in a particular topic. As a result this projection doesn't have to be what you necessarily want the world to be like or not be like, but through opening up a certain issue and provoking responses from people the building of futures can become more inclusive. This inclusivity is what was missing in modernist architecture as I alluded to earlier, and future scenario building therefore can be influenced be different participatory design practices and methods to engage people in these conversations.

<!-- Include image here - maybe from class exercise or something, or the Manoa thing...Or something from their website
-->

![]({{site.baseurl}}/break10.png)

### AI & The Future of Creativity

Artificial intelligence has influenced future thinking and notions of the future for over half a century and developments in machine learning in the past decade or so has  enabled the field to develop exponentially resulting in a significant impact on future thinking and projection. This is a result of the way in which machine learning as a method is used as a tool to make better and more accurate predictions which is now felt through the whole of society, whether its the film you choose to watch on Netflix or the route you choose to walk home from work.

Themes such as the future of work and automation, and the possible development of an artificial general intelligence are also a huge part of the conversation surrounding future thinking and artificial intelligence. These are some themes I have touched upon in previous posts ([<u>Navigating the Uncertainty</u>](https://mdef.gitlab.io/oliver.juggins/reflections/navigating-uncertainity/) and [<u>Designing with Extended Intelligence</u>](https://mdef.gitlab.io/oliver.juggins/reflections/designing-with-extended-intelligence/)) which led me to my current theme for my project which broadly involves the human - machine relationship but focuses in on the role of creativity and artificial intelligence. We are often reminded that our jobs are at risk due to artificial intelligence, and there seems to be an assumption that the creative industries will be immune to any influence from artificial intelligence. I think the creative process will be affected by artificial intelligence and that learning about AI can enable humans to explore their own creativity in new ways. How will machine learning affect the arts is a question I am keen to investigate and how artificial intelligence can be used as a tool to explore new means of artistic production and the human - machine relationship.

<u>Humans & Machines</u>

As machines become a greater part of our lives I think it’s important we understand more about them. I believe a way to engage people in this area could be through linking artificial intelligence and the arts. This investigation also presents an opportunity which is already being explored in using machines and artificial intelligences as creative collaborators which allow humans to express themselves in novel ways. I think this raises some interesting questions surrounding working with machines regarding authorship and can help humans fulfil new potentials in being able to find out how machines can help them and can also shed light on what it is that makes us human in the first place. [<u>Sougwen Chung</u>](http://sougwen.com/) is an artist who is exploring their own relationship with machines through creating work with machines through mark making and sculpture.

{% figure caption: "*Sougwen Chung's Omnia per Omnia, 2018*" %}
![]({{site.baseurl}}/creativity.jpg)
{% endfigure %}

<u>Future of Music</u>

<p>
<font size="5">With changing tools and ever more powerful algorithms that automate tasks from simple to advanced, the daily life of an artists will change a lot, just as in any other field where machine interaction is involved. This finally gives us the opportunity to focus on the human, random, bodily, performative part of being creative. And maybe someday with the help of machines we will find out what actually makes us human after all. - Mortiz Simon Geist, 2018</font>
</p>

My interest in music has led me to look analyse the role of AI in how it will impact music production and experience of consuming music, and is a space which I believe the human - machine relationship can be examined and developed. For over half a century algorithmic music has been around, but it is only within the past couple of years that AI generated music has started to enter the mainstream. There are huge implications for how machine learning with affect the music industry and the growing number tools for computer generated music is growing year on year. Moritz Simon Geist is a performer, musicologist, and robotics engineer who is interested in the role humans will have as AI begins to carry out more of the tasks that were once solely in the domain of humans. He is interested in where humans can intervene and participate in the process of experiencing and creating music. He believes humans should focus on the qualities that are not replicable with machines and that we can learn from this process as a result.

<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/wHrCkyoe72U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<u>Detroit Techno & Jeff Mills</u>

Techno is often described as the ‘music of the future’ and its origins can be traced back to three high schoolers from Detroit known as the ‘Belleville Three’: Juan Atkins, Kevin Saunderson and Derrick May. Since its inception, techno has been associated with visions of the future. Jeff Mills, one of the early innovators of techno and one of the most significant figures in in electronic music cites the sci-fi magazines that were on every street corner of Detroit as inspiration for the creation of his music. Experimentation with machines and exploring what the technology provided by synthesizers and drum machines is ingrained in techno. The [<u>Korg MS-10</u>](http://www.vintagesynth.com/korg/ms10.php) is one of the iconic synthesizers which played a significant role in some of the early techno tracks in the mid 1980s. [<u>Metroplex</u>](http://www.metroplexrecords.one/) Records was one of the first labels to come out of Detroit, founded by Juan Atkins which released one of the classic detriot techno tracks as Model 500, No UFO’s.

<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/KNz01ty-kTQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<p>
<font size="5">I’m not making music just for the weekend, I am making music for generations to come.’ - Jeff Mills, 2018</font>
</p>

[<u>Jeff Mills</u>](https://www.axisrecords.com/) is considered by many a futurist and future thinking has been the driving force of his work as both a DJ and producer. He tries to express visions of the future through music and sounds, in ways not possible through words and is interested in how electronic music can become a wider social and cultural phenomenon. On talking about these projections of the future and the creation of future scenarios in an interview with Mariana Berezovska he describes his process: ‘Pre-dating creativity or creating something to be experienced not now, but at s future date is something I think about more and more. The advantages are that it provokes people, myself included, to think about the future and the time it takes to get to that date.’

His discography is vast and varied which ranges from his iconic [<u>‘The Bells’</u>](https://www.youtube.com/watch?v=DwpedKWwS3w), to a [<u>rewritten score</u>](https://www.youtube.com/watch?v=DvBd1X1Qk-g) of Fritz Lang's Metropolis. In an interview at Fabra i Coats Arts Centre in Barcelona he talks about his Sleeper Wakes performance at Razzmatazz in 2010, his inspirations growing up in Detroit and how he uses music and performance to tell a story and convey his ideas about the future.

<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/NT000d--gpE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


<!-- Maybe can include bit about music here & Jeff Mills. The future of music
Talk about Mortiz Simon Geist here and Jeff Mills, and talk about techno, electronic muic and the future of electronic music and how notions of the future are what techno came from. And the idea of experimentation with machines.
electronic music has explored the relationship of human machines, and is embedded within its development. Put in a classic detroit techno track, nights of the Jaguar and something by jeff.
-->

![]({{site.baseurl}}/break10.png)

<!-- Maybe can include bit about music here & Jeff Mills. The future of music-->

### Speculative Timeline

As an exercise I carried out a study on AI & creativity through documenting and speculating on the technological, social, experiential and stylistic developments in music between 1950 - 2050. I chose to look to the past to try and gain a better perspective and contextualise the way that technology has shaped music and how the experience of consuming music has evolved as a way of forming speculations. Hover mouse over image to look in greater detail.

<img
 class="zoomable_image" src="{{site.baseurl}}/speculationsmall.jpg" data-zoom-image="{{site.baseurl}}/speculationlarge.jpg"
 />

 <script>
 $(".zoomable_image").elevateZoom({
   zoomType				: "inner",
   cursor: "crosshair"
 });
 </script>

 ![]({{site.baseurl}}/break10.png)

### Reflection

As the title of the course describes, the week was an introduction to futures and I now feel I have a really good starting point to go and carry out further research and reading about thinking about the future as a theme in general but also for my area of interest. The week helped me to develop my ideas about creativity and artificial intelligence and the different considerations that I learnt about when thinking about projecting future scenarios can be applied to this theme and help me develop it further. The workshops during the classes and the different practical exercises that can be done to develop future scenarios is also something I plan to carry out for my area of interest such as the two axis matrix, future’s wheel and manoa’s exercise. I felt that the week also tied together a lot of the previous weeks content such as [<u>Living with Ideas</u>](https://mdef.gitlab.io/oliver.juggins/reflections/be-your-own-system/) through building on and contextualising speculative design. This is an approach that I am keen to explore more as the masters progresses feel that with more of the background knowledge and techniques that I was exposed to this week I will be able to apply some of the core ideas related to speculative design to my own work more effectively.

### References & Further Links

* [Review the Future](http://reviewthefuture.com/) podcast
* <http://postnormaltim.es>
* [Situation Lab](http://)
* [Superflux](http://superflux.in/)
* [What Future Studies is, and is Not](http://www.futures.hawaii.edu/publications/futures-studies/WhatFSis1995.pdf) by Jim Dator
* Borsch magazine issue 3, edited by Jeff Mills
