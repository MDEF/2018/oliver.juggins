---
title: Emergent Business Models
period: 16-30 May 2019
date: 2019-06-03 12:00:00
term: 1
published: true
---
Emergent Business Models was a seminar led by  Javier Creus and Valeria Righi of [<u>Ideas for Change</u>](https://www.ideasforchange.com/). The classes were formed of different exercises that helped develop a business model for the project which I will outline in this documentation.

Before doing so however I will provide some context to their work based around the [<u>pentagrowth model</u>](https://www.ideasforchange.com/pentagrowth) which is a body of research which came from the studying exponential growth in organisations. The model identifies five ‘exponential growth levers’ that together bring “a new point of view on the keys that can foster the growth of organizations in the digital environment by combining your internal assets with those available in your business ecosystem”. The model came as a result of analysis carried out by Ideas for Change on 50 organisations which have grown more than 50% in the number of users and in terms of revenue for 5 consecutive years in a period from 2008. These organisations include Netflix, Dropbox and Arduino.

I will briefly summarise the different branches, or ‘laws’ that describe the different elements of the pentagrowth model:

* Connect: Through an organisation having more connections (people, situations, things) the potential for growth is higher.
* Collect: An organisation that has to put in less effort in terms of creating and building its inventory, the greater is its potential for growth is as its energy can be focused elsewhere.
* Empower: Users that are empowered and are integrated into the business model provide more potential for growth.
* Enable: Having a higher number of people that can create value for an organisation through the organisation providing the necessary tools, the potential for growth is greater.
* Share: Where there is a community around an organisation that feels a sense of ownership of the organisation and its content, there is a greater potential for growth.

Key questions that will inform the following pieces of analysis and drive each of these laws can be categorized and summarised as:

* Connect: What network do you design for?
Collect: How can you add new units of value and build your available supply reducing internal effort?
* Empower: What capacities of your users could you integrate in your organization / production processes?
* Enable: What tools can you provide to third parties so they can generate value?
* Share: What unused content does the company have? What communities are already paying to access to a similar content?

The first exercise consisted of making a presentation which involved defining the ‘impact variable’, ‘system’ in which the project is based and carrying out a series of case studies, analysing related innovations to my project and then carrying out some reflections . Here is the presentation:
<br>
<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vSKv2TbhX6Ns-51dETPBkpxCbNeneQckfcSKdxFdzWjf32tQGlQ4XiYUO78FLxbE3TIV9iOapSfekb8/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="360" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

The next class was based around applying the principles of the pentagrowth model to our projects in the form of a newspaper article. Here is my article:

<br>
<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTM-j3dyB-yZyyTI0lc5fdd5i4ddQ5p5P1i8Lj1bDHMmfUCepEPSHoTBuiKEOzzC38hEfad-kTyNS6g/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="922" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

These exercises and seminar really helped me to develop a business model for my project over the next 5 years and the different services I can offer which will be through workshops, the sale of ‘kits’ and an online platform. Analysing past innovations was really a good exercise and I am fortunate in that there have been various extremely successful companies in the digital literacy field I can look to for inspiration.

## Proposed Business Model & Services

The workshops will continue to take two formats: workshops that I give to a class of children myself and the workshops I will give to teachers. For interested collaborating teachers that would like me to give workshops in their school, I will then propose a range of workshop plans that they can choose which include:

* One off workshop to a class of students introducing the basics of machine learning (2 hours).
* Series of workshops given to students, initially introducing the basics of machine learning but then incorporating physical computing (2 x 2 hours).
* Workshop series given to students incorporating machine learning, physical computing, digital fabrication (depending on the available resources and facilities) and the development of a personal project (3 or 4 x 2 hours).

The range of workshops I propose for teachers will be tiered in terms of ability with the idea being that teachers can complete the whole range of workshops in order as the difficulty of the classes increases:

* Beginner - Introduction to machine learning including 3 activity and lesson plans (2 x 2 hours).
* Intermediate - Physical computing and machine learning (2 x 2 hours)
* Advanced - Subject of teacher’s choice relating to their syllabus and machine learning, co-designing activities (3 x 2 hours).

Here is a discussion of my business plan in the context of the pentagrowth model:

The plans in years 1-3 aims to ‘connect’ with a wide range of people and organisations such as teachers and 'FixEd' based in London. By forming collaborations and connecting more people to the project, a network will be able to begin to grow more effectively. I believe this will be necessary in both the service element of the business model in collaborating with teachers and the Future Learning Unit for example and the sale and development of kits.

A way which would ‘empower’ people that interact with the project would through incorporating teachers into the design of workshops, taking advantage of the capacity for teachers to contribute their ideas and creativity. This would begin to create a community around the project and ‘enable’ more value creators to share the lesson plans and activities which could grow the number of users for business and its potential to grow.

The certification type system will empower users and help start to build a community around the project. As more teachers participate in the project, they will then be able to contribute ideas on lesson plans and activities. Through having a greater number of value creators and developing a way whereby people that contribute to the business can be rewarded will create a ‘shared’ sense of ownership, the potential for growth of the business will be greater.

The goal of year 3-5 is creating an online platform and transferring the community around the project built up through the workshops carried out has the potential to make the project grow at a fast rate since there could be a critical mass of people ready and willing to start an online community. By making course content open and free to use and modify more teachers would be able to start bringing machine learning to the classroom and spread this knowledge. This sharing will empower teachers and students and could result in enabling partners. With a large user base that believes in the project other organisations may be willing to collaborate to manufacture and sell kits related to different content and activities. littleBits, which I discussed in chapter 4 did exactly this through collaborating with Korg in their Synth Kit.
