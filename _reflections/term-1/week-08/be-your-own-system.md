---
title: Living with Ideas
period: 19-25 November 2018
date: 2018-11-19 12:00:00
term: 1
published: true
---
*Tutors: Angella Mackey & David McCallum*

Living with Ideas was a week which opened up the world of speculative design and equipped me a range of tools and techniques that I can use throughout the masters. It was a challenging week which required an open-minded approach and was a totally approach for me. The week developed my area of interest and I experienced a new way of thinking which will help me as a designer for life beyond IaaC. A series of workshops were led by designers [<u>Angela Mackey</u>](https://www.angellamackey.com/) and [<u>David McCallum</u>](https://www.sintheta.org/portfolio/), currently both carrying out PhDs with research areas at the intersection of technology, art and design. They are also concerned with using design methodologies relating to working with emergent technologies which formed part of the classes and workshops.

![]({{site.baseurl}}/break10.png)

### Counterfactual Artifact

Although I had heard of the term speculative design and am aware of some design studios working in this area such as [<u>Superflux</u>](http://superflux.in/#), I was unsure how I could relate this approach to my own design practice. An [<u>essay</u>](http://interactions.acm.org/archive/view/march-april-2016/a-short-guide-to-material-speculation-Actual-artifacts-for-critical-inquiry) helped my understanding of what speculative design encompasses what is meant by terms such as material speculation and what a counterfactual artifact is. These terms related to the first workshop of the week, where the activity was based around creating this counterfactual artifact. This involved asking a ‘what if’ and modifying an object imaginging what it would be like in this speculated scenario. This adapted future-oriented object or device is then used in order to gain some insights and reflections through using it over a period of time as primary research.

{% figure caption: "*Selected object for exercise*" %}
![]({{site.baseurl}}/mirror.jpg)
{% endfigure %}

<u>What if there were no mobile phones?</u>

I imagined a future where there were no longer mobile phones, and what a mirror would be like in this world. I thought that mirrors could evolve into communication devices, and I could make use of the way they reflect light for sending messages through light and shadow. The mirror I had was small, and worked well as a large necklace with movable ‘hands’ on its face in order to create different patterns when light was reflected.

{% figure caption: "*Device as a wearble*" %}
![]({{site.baseurl}}/necklace.jpg)
{% endfigure %}

I wore the device for about 30 minutes, but it was within the first 5 minutes I gained my most valuable insight. Changing contexts completely altered my experience of using the device and its effectiveness. This was because inside the classroom there were lights that could help form shadows on walls and tables but outside there was direct light source for the mirror to reflect, or even a suitable surface to reflect onto. The idea of actively seeking new contexts for a project and trying to imagine contexts which a project might not function could be a really useful exercise and was the main thing I learned from this exercise.

{% figure caption: "*How messages might be sent using the device*" %}
![]({{site.baseurl}}/symbol.jpg)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Wearing an Idea

The next activity opened up another technique aimed at generating ideas for a project through confronting it and living with it, all based around the notion of working with a technology that does not yet exist. I learned that this can be done through breaking down the essence of what the project is about and using existing technologies to mimic the desired effect in some way, and then seeking first-hand experience to translate the idea to the present day. This technique is the basis of Angella’s current research and during the workshop I applied her research methods with the techniques from her own project which explores a kind of digital fabric where clothing acts as a green-screen for selected images.

As a class we experimented with this technique and I selected images of environmental conditions and natural landscapes to be displayed on my clothing, imaging a future fashion trend where skies and clouds are ‘in’. I generally do not wear much colour but thought projecting the sky might be a nice and subtle way to use imagery on clothing. It was a fun to play with the ChromaKey app  and project images onto myself, but I think I would personally prefer a technology like this to be applied to accessories which are not as invasive such as a laptop sleeve or scarf.

{% figure caption: "*Cloud-scarf*" %}
![]({{site.baseurl}}/scarf.jpg)
{% endfigure %}
![]({{site.baseurl}}/blank.png)
{% figure caption: "*Skies projected onto laptop case*" %}
![]({{site.baseurl}}/laptop2.jpg)
{% endfigure %}

I am interested to see where a technology like this might be headed with the development of augmented reality and ubiquitous computing. Clothing as interfaces and screens could be interesting, as well as buildings and the city as a whole. Having said this I also see less appealing applications where people become moving billboards with advertisements projected onto them who are paid for this service. At least for now this situation can be avoided by not wearing anything green.

![]({{site.baseurl}}/break10.png)

### Magic Machines

The third and final technique used to develop speculative projects during the week's workshops was the creation of some ‘magic machines’. David ran this activity which was about using making as a way to generate ideas and make new discoveries which would not be possible through desktop research alone. The process of making a magic machine consists of selecting one of the human desires and responding to that desire through producing a physical model.

I chose the desire tranquility which I associated with balance and equilibrium. I created a machine entitled TranquilliRead which is a device to read the tranquility of an individual or situation. I thought movement could be an indicator of tranquility and imagined what tranquility looked like in physical form. The device references the form of a windmill and the magic is applied through the device moving when measuring tranquility.

{% figure caption: "*TranquilliRead in action*" %}
![]({{site.baseurl}}/tranquility.jpg)
{% endfigure %}

This method was then applied to my area of interest and I selected the desire acceptance. I chose this desire as my area of interest is concerned with the relationship between humans and machines I thought acceptance is an appropriate feeling for humans as machines become a greater part of our lives and automation increases and displaces people from work. Instead of fighting this displacement we should accept it, and see it as an opportunity to do activities machines are unable of carrying out and instead learn to work with machines in the most effective way. My magic machine reflected this through an armband which would act as an interface between humans and machines which is able to pair you with an AI best suiting your personality and skills helping you to fulfil your full potential.

{% figure caption: "*Human - Machine pairing device*" %}
![]({{site.baseurl}}/acceptance.jpg)
{% endfigure %}

Since creating this magic machine I came across a speculative project by IDEO called [<u>The Purpose Compass</u>](https://www.ideo.com/blog/what-the-ai-products-of-tomorrow-might-look-like) which is an AI tasked with helping humans prosper in a machine-augmented future.

<br>
<iframe src="https://player.vimeo.com/video/268483647" width="680" height="360" frameborder="0" allowfullscreen></iframe>

![]({{site.baseurl}}/break10.png)

### What if AI could grow?

Throughout the week before the groups of interests in the masters met to discuss and develop ideas together. I was in a group concerned with the translatability between systems and we developed a speculative project which captured some of our shared interests. The systems we were interested in translating between included the organic, digital and artificial systems. We combined the techniques from the first two workshops to create a project of our own exploring an interface between humans and an AI which exists as part of the natural ecosystem. We thought about what an interface which connects humans to the natural world would be like and how our relationship with the organic system would change as a result.

We saw a parallel between the veins of a plant and the veins on our hands and how this could form the basis of communication between these systems. Adapting some latex medical gloves we speculated on how this interface could function and discussed our ideas as a group before quickly prototyping and testing how the experience would be with an actual plant.

![]({{site.baseurl}}/hand1.jpg)
![]({{site.baseurl}}/blank.png)
![]({{site.baseurl}}/hand2.jpg)

We ‘lived’ with this idea for a couple of hours but soon realised that something more subtle and embedded within the skin like a tattoo would be more comfortable than a glove. Applying the idea of this technology in the present day could be possible through using QR codes and smartphones as means of communication. Incorporating this technology with our devices would have been the next step to push the idea further.

![]({{site.baseurl}}/break10.png)

### Algorithm Comparison

The remaining workshops in the week were dedicated to developing individual areas of interest using the techniques from the previous days. My interest in computer vision and machine learning led me to carry out two activities where I compared myself to an algorithm and another where I invited someone to become an algorithm.

I compared myself to an algorithm through selecting a series of images, writing a brief description of the scene and a series of words that I associated with that particular image. I then fed each of the images into an online image recognition piece of software called [<u>clarifai</u>](https://clarifai.com/demo) which generated a list of words with a percentage of accuracy for each word. Here is an example from one of the images I used with the my interpretation on the left and the algorithm’s interpretation on the right.

![]({{site.baseurl}}/humanmachine.png)

Directly comparing myself to this image recognition algorithm exposed the inherent differences between humans and machines in the perception of images. The algorithm interprets the image as data, breaking the image down into a series of descriptive words which are a result of previous images that is has seen. To some extent I interpreted the image in a similar way as I have learnt what people playing football looks like over time which makes me able to understand what I see in the image. I was able to do this better than the machine I can say what is in the image with absolute certainty what is in the image which the algorithm is not able to do. However, the algorithm is better at interpreting the colours in the image as it can provide an exact colour hex code which I am unable to do. The idea of comparing ourselves to machines as a way of understanding them could be applied to thinking of ways we could best to collaborate and make the most of the different qualities we have.

An important difference to highlight in my interpretation however us the way in which I associated the memories with the image and my emotional response. When I saw the image I understood it as a collection of different elements that I could identify but I also made associations with what I saw which transcended description. Seeing a group of teenagers playing football triggered something which made me think of when I was that age and played football everyday in the local park with friends and made me think about that time in my life. This is a very personal and individual response to an image that a machine is unable to replicate.

![]({{site.baseurl}}/break10.png)

### Emotional Machines

How I make sense of the world through what I see is not binary and my sensory responses are built up from my past, but the way a machine interprets the world is like a collection of other people's past experiences, or the past experiences of those who created the algorithm. I thought about if emotion is something that can be replicated in a machine or if it should be replicated at all? Do we even fully understand the human experience to be able to replicate in the first place, and if it could be replicated what does that mean for humans?

Luiz Pessoa, a professor of psychology and director of the Maryland Neuroimaging Centre argues that emotion is something that should be built into machines in his [<u>essay</u>](https://aeon.co/ideas/robot-cognition-requires-machines-that-both-think-and-feel) addressing robot cognition and emotion. He writes ‘without emotion, no being that we might create can have any hope of aspiring to true intelligence.' Pessoa sees cognition and emotion as qualities that should not be separated when building intelligent machines and that emotion itself is an important part of intelligence because it helps us make connections and the process of piecing together of information. Through understanding human emotion as something which is an integral part of our ‘cognitive machinery’ and that the areas of the brain associated with perception, cognition, emotion and motivation and bodily sensations are closely intertwined, how they can be incorporated into machines can be better understood. Emotion shouldn’t be an add-on feature as it is currently considered in the design of intelligent machines, but is something that should be connected to everything within a cognitive system.

### Being an Algorithm

The second experiment I carried out was an activity I did to test the idea of learning about a particular technology through a designed experience. This was not necessarily an exploration into living with an idea, but rather something I am interested in exploring for my final project and it felt like a good opportunity to try during week. The experience I wanted to try and recreate was how a machine learning image recognition algorithm works through actually becoming an algorithm. I created a device with the counterfactual artifact activity in mind, adapting some glasses which had a tray for displaying images. The idea is that someone wears this device and has to create their own training set through the images provided. I asked that participants created two piles of images determined by the presence of a bicycle, and this would form a training set of images.

{% figure caption: "*Sample images selected for interpretation*" %}
![]({{site.baseurl}}/bicycles.png)
{% endfigure %}

I noticed each person I asked to use the device at some point was uncertain about the decisions they were making in their selection of images. This was my intention as I selected images that were ambiguous as to whether there was a bicycle in them or not. I did this to try and demonstrate that algorithms work under binary logic and there is no room for discussion in the selection of an image, only a yes or no is possible or ‘bicycle’, ‘no bicycle’. When becoming the device there is no space for debate or uncertainty as it only allows the you to make decisions in a quantitative manner.

{% figure caption: "*A device for analogue machine learning*" %}
![]({{site.baseurl}}/mlglasses.jpg)
{% endfigure %}

The results of the experiment were quite revealing in that each person had created a unique training set. This difference in interpretation shows how important it is for human presence in the creation of these training sets and for multiple people to review them once they have been created. We all interpret images differently, even in something as simple as identifying a bicycle.

{% figure caption: "*Being an Algorithm*" %}
![]({{site.baseurl}}/nico.jpg)
{% endfigure %}

Although the activity was not ‘living’ with an idea, I still found the exercise really useful and learnt a lot from it. It strengthened my desire to use the technique of experiencing a technology as a way of learning for my final project. It also made me think more deeply about how hard interpreting images are for humans and the issues that can be faced when giving away this responsibility to a machine. It is clear that although machines can be incredibly useful in processing large amounts of data very quickly, humans must continue to engage with the process of interpretation and understand how machines make decisions for us. As Alexander Strecker writes in an [<u>essay</u>](https://www.lensculture.com/articles/trevor-paglen-the-interpretation-of-images-retaining-our-humanity-in-the-age-of-artificial-intelligence) about the work of [<u>Trevor Paglen</u>](http://paglen.com/) ‘as we hand over more and more tasks to machines, we can’t let ourselves be driven solely by logics that demand the most expedient solution and the most immediate interpretation.’

![]({{site.baseurl}}/break10.png)

### The New Normal

A talk given by [<u>Nicolay Boyadjiev</u>](https://strelka.com/en/education/people/nicolay-boyadjiev) from the [<u>Strelka Institute</u>](https://strelka.com/en/home) at IaaC during the week came at a really good time in relation to the themes and ideas discussed during Living with Ideas. The lecture discussed projects from the [<u>The New Normal</u>](https://thenewnormal.strelka.com/), an experimental postgraduate programme from the Strelka Institute described as a speculative think-tank which focuses on the impact of emerging technologies on contemporary urbanism. It was really useful to see how the projects dealt with making the connection between speculation and reality and will serve as a source of inspiration for my own work. Research projects have been extremely varied in its scope, from urban design tools driven by deep learning to cryptocurrencies and new models for home ownership. Learn more about the different projects [<u>here</u>](https://thenewnormal.strelka.com/research).

![]({{site.baseurl}}/newnormal.jpg)

![]({{site.baseurl}}/break10.png)

### Reflections

Applying the techniques Angella and David exposed me to during the week to my area of interest was really useful in that it forced me to be specific in the area I would like to address within the human - machine relationship. I learned a lot from the results of my experiments, but still feel I can develop my methods a lot more and think of an activity which will enable me to live with an idea to test more thoroughly. These activities also made me realise I need to better understand my relationship with machines and perhaps even change my relationship with machines to develop my project. I still have a very superficial technical understanding which I need to address in order to take my project forward and I feel I need to understand a lot more about how the technology works through immersing myself in the world of machine learning. I will do this through engaging with it and trying out some of the different online resources I discovered during [<u>Designing with Extended Intelligence</u>](https://mdef.gitlab.io/oliver.juggins/reflections/designing-with-extended-intelligence/) which will serve as some primary research and hopefully take my technical understand to a much higher level.

The connection between speculation and reality was made a lot more tangible as the week progressed and having more time to live with an idea as the masters progresses will be useful. I will aim to try this technique as much as possible throughout the year as it can give really quick feedback and is easy to carry out as it only depends on yourself participating. The week also cemented and clarified my area of interest, although it also made me realise it is still quite broad and needs to narrow down as the human - machine relationship can be explored in many different ways. I am also keen to continue to develop ideas for experiencing processes related to machine learning and artificial intelligence as the idea of using engagement as a way to open up a new technology is something I would really like to incorporate into my work.

![]({{site.baseurl}}/break10.png)

### References & Links

* [A Short Guide to Material Speculation](http://interactions.acm.org/archive/view/march-april-2016/a-short-guide-to-material-speculation-Actual-artifacts-for-critical-inquiry) by Ron Wakkary, William Odom, Sabrina Hauser, Garnet Hertz & Henry Lin
* [The Purpose Compass](https://www.ideo.com/blog/what-the-ai-products-of-tomorrow-might-look-like)
* [Clarifai](https://clarifai.com/demo) by IDEO
* [Robot cognition requires machines that both think and feel](https://aeon.co/ideas/robot-cognition-requires-machines-that-both-think-and-feel) by Luiz Pessoa
* [The Interpretation of Images: Retaining Our Humanity in the Age of Artificial Intelligence](https://www.lensculture.com/articles/trevor-paglen-the-interpretation-of-images-retaining-our-humanity-in-the-age-of-artificial-intelligence) by Alexander Strecker
* [The Strelka Institute](https://strelka.com/en/home)
* [The New Normal](https://thenewnormal.strelka.com/)
