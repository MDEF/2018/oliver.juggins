---
title: From Bits to Atoms
period: 10-16 December 2018
date: 2018-12-16 12:00:00
term: 1
published: true
---
*Tutors: Santi Fuentemilla, Xavi Dominguez & Eduardo Chamorro*

From Bits to Atoms was an intensive and enjoyable week which served as a primer and sneak peak of what to expect during the [<u>Fab Academy</u>](http://www.fabacademy.org/) starting next term. In terms of setup and delivery for the week, there were similarities with [<u>The Way Things Work</u>](https://mdef.gitlab.io/oliver.juggins/reflections/the-way-things-work/) in that there was a main project to develop for the duration of the week, with accompanying class time covering theory and instruction on how to use the machinery in the Fab Lab. There is a dedicated [<u>website</u>](https://bitsandatoms.gitlab.io/site/) for the course including the syllabus and various resources and is something I will be referring back to during the Fab Academy. This will be especially useful for the content regarding guidelines using the pieces of equipment which was covered in a series of talks given by Eduardo.

![]({{site.baseurl}}/break10.png)

### Scribble Sound

The brief for the project was to make a machine that produces something with some form of interaction or movement with inputs and outputs. Because the brief was so broad it was really nice to essentially be able to spend the week building something on whatever you were interested in and wanted to learn more about. For me this was music, and I worked in a group with Gabor, Ryota, Julia, Silvia and Saira on a music machine entitled ‘Scribble Sound’. The concept and initial intention which we developed on the first day of the week for Scribble Sound was to create a machine that would produce music in real-time responding to drawing and mark-making. I am interested in developing something involving music for my final project, so was really happy to be able to work on a project of this theme for the week.

A reference for the project from our first conversation was Axel Bluhme’s [<u>Xoxx Composer</u>](http://xoxxcomposer.axelbluhme.se/) and incorporated the idea of having different tracks which rotated vertically. As a group we made some sketches and a rough physical model to show how the overall massing of the machine would be and where the key components would go. We also thought about how exactly the idea of being able use drawings as a way to produce sound would work but as we learned this was best explored through experimentation and trying things out. As well as using making as a way to explore our ideas from the beginning of the project we also tried to get as much insight into the different components and tools to use from the team in the Fab Lab and tutors who were an invaluable resource throughout the week.

{% figure caption: "*Initial sketches of proposed machine*" %}
![]({{site.baseurl}}/sketches.png)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Design Development

My role in the project was focused on the 3D design development and construction of the machine. I had previous experience preparing files for the laser cutter and using rhino for 3D printed parts from [<u>Design for the Real Digital World</u>](https://mdef.gitlab.io/oliver.juggins/reflections/design-for-the-real-digital-world/) so was able to prepare these parts for assembly and understood the tolerances needed for the different pieces of equipment. Despite this experience there were still some issues with tolerances when putting the various pieces of the machine together, but was nothing that some improvisation and sanding couldn’t resolve. The files for the laser cut elements and 3D printed spindles are in the [<u>repository</u>](https://gitlab.com/MDEF/scribble-sound/tree/master/public/cad%20files) for reference and have been updated to have the correct tolerances for future versions of Scribble Sound.

{% figure caption: "*CAD development*" %}
![]({{site.baseurl}}/cad.png)
{% endfigure %}

The design process in general was fast paced, and I learned a lot from the approach that was required to develop the project which will really help me to develop my own projects in the future. In the past when working on similar types of project I have definitely been guilty of trying to solve all of the problems on paper, and to plan for everything imagining a final piece or product which fits together perfectly without properly testing the individual parts. In reality there are always problems that need to be solved along the way, because there are so many opportunities in the hardware, software and assembly phase things to go wrong, so starting small and building up the complexity gradually of what you are doing is really important.

{% figure caption: "*Testing materials for the tracks used*" %}
![]({{site.baseurl}}/tracks.png)
{% endfigure %}

This is one of the main things I learnt during From Bits to Atoms, and will be really important for me to keep in mind for the Fab Academy. In developing Scribble Sound, we applied this mentality by adding one track at a time and making sure that the sensors were responsive on their own first without adding them to the structure. This was useful as inevitably things didn’t always work the way we envisaged and it made it easier to adapt and make changes along the way so that when everything had to be combined in the final structure it was a lot easier.

<!-- Screenshots of various 3D models etc -->

I would have liked to have spent more time developing the circuitry and code for the project, but as is often the case when time feels of the essence and a quick turnaround is necessary it is hard to go out of your comfort zone. The great thing about group work however is being able to learn from others, and by virtue of working on the project I gained insight into what was happening with the electronics and programming. I also asked Gabor at the end of the week to talk me through the different parts of the code (in the [<u>README</u>](https://gitlab.com/MDEF/scribble-sound/blob/master/README.md) file to understand everything that was going on including how the sensors worked and how the arduino was able to produce sound using [<u>Hiduino</u>](https://github.com/ddiakopoulos/hiduino). During the Fab Academy there will be no escape from getting involved in every aspect of the project development so am looking forward to developing my electronics and programming skills over the next two terms.

{% figure caption: "*Testing the ele and assembly*" %}
![]({{site.baseurl}}/process.jpg)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Key Learnings from the week

As well as the insights I gained in the design development aspect of the project, here is a brief summary of other key learnings from the week.

<u>Gitlab</u>

Although I have been using Gitlab from the first week of the masters, it was in doing a group project this week which felt like I was using it to its potential through collaborating with others to add information to the repository and update the website. There might have been a couple instances of lost work and merge conflicts, but nothing that couldn’t be easily resolved and we all learnt how to work together on the project remotely within the first day or so. Having this experience has also helped me navigate the world of Gitlab and Github a more easily which is useful for researching other projects I have come across such as the [<u>NSynth Super</u>](https://github.com/googlecreativelab/open-nsynth-super) project developed by Google.

<u>Debugging</u>

A useful tool I learnt about for developing a project virtually and for the debugging process is the various online applications and pieces of software for simulating circuits such as [<u>Frtizing</u>](http://fritzing.org/home/). Just like in The Way Things Work, I discovered more tips and tricks for working out what is going wrong and why something isn’t working and one of the best ways is observing other classmates and tutors in how they solve problems and try and understand their process.

<u>Know your Equipment</u>

Another important thing I learnt was to understand the differences in the pieces of machinery being used in the Fab Lab. This affected some of the 3D printing that was done in the project as the file was prepared in the same way for a different 3D printer which proved problematic as this machine dealt with overhangs differently causing the job to not complete properly. This mentality can be applied to all of the different pieces of equipment in the Fab Lab, and will be important to keep in mind to save time and material for future projects.

<u>Fab Lab & Working Independently</u>

Just by spending more time in the Fab Lab it was an opportunity to learn more about how to work in the space and the different hand and power tools on offer. This was useful because as much as I enjoy using the digital fabrication tools, I think things should be done by hand when possible to save and is useful to be able to combine the handmade and the digital. The setup of the Fab Lab is perfect for working on group projects and developing ideas collectively and have found it a really good environment to learn from others in.

I also found the way the week was structured a good way to learn and the guidance from tutors worked really well through giving us space to work independently. I have found that working things out on your own is a really good way to learn, so having being able to check in with the tutors and ask for their assistance when necessary is a good setup. I have made a mental note to always try and get advice however as even when I have thought I am doing something that works well, there have always been suggestions and bits of insight that improved the project in some way.

{% figure caption: "*Sensors sat just below tracks to detect the black marks*" %}
![]({{site.baseurl}}/testing.jpg)
{% endfigure %}

![]({{site.baseurl}}/break10.png)

### Reflection

Some final reflections on the week are that the project ended up linking to my area of interest through exploring new ways of making music through interaction. In my final project I would like to incorporate AI, but there is still a lot I have gained from the week I will be able to incorporate into my own work. I should also point out that ultimately we were not actually able to use drawing as a way of triggering sounds, but I feel there was definitely proof of concept and the first iteration is a good first step. With further development and testing drawing could be implemented successfully, and from the discussion at the final presentation there were ideas for developing the project further such as making a variety of lengths of tracks or including a potentiometer to change the speed to of the track.

The week also was another opportunity to put into practice the ideas and theory of working on a project with the open source mentality, which is both important for Fab Labs and IaaC in general. Each group having a repository was a nice idea to be able to not only see what everyone was doing during the week, but also provide a record for future reference. I plan to develop my own final project to be open source during the masters and will spend time seeking out more projects which have been documented this way for my own reference.

<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/BS5BNDIuOpk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![]({{site.baseurl}}/break10.png)

### References & Further Links

* [Scribble Sound Repository](https://gitlab.com/MDEF/scribble-sound)
* [Scribble Sound site](https://mdef.gitlab.io/scribble-sound/)
* [From Bits to Atoms](https://bitsandatoms.gitlab.io/site/) course website

<u>Software</u>

For personal reference here is a list of pieces of software which can be used for developing projects involving sound which might be useful next term:

* [<u>Mozzi</u>](https://sensorium.github.io/Mozzi/) - Sound library for Arduino
* [<u>Sonic Pi</u>](https://sonic-pi.net/) - Live music coding
* [<u>Hiduino</u>](https://github.com/ddiakopoulos/hiduino) - Native USB-MIDI on the Arduino
