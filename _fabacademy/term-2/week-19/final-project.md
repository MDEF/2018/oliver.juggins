---
title: Final Project
period: 5 June 2019 - 14 June 2019
date: 2019-01-24 12:00:00
term: 2
published: false
---
Do this from 15-19th June

As part of the deliverable for the master project, I have made a catalogue to include different exercises to for classes to do. Beginner / medium / advanced.

The prototype for the advanced can be documented as a final project for the Fab Academy. Wanted to have a record, and combine the different skills I have picked up through the year.

Include a Google Slides.

Skills - 3D printing. Laser cutting. Network and Communication. Applications & Implications. Computer Aided Design. Input & Output devices (kind of).

## Useful links
