---
title: Computer-Aided Design
period: 30 January - 06 February 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
The assignment this week involved getting to grips with 2D and 3D modelling, trying out some different programs, experimenting and building the idea of the final project of the Fab Academy in 3D and rendering and animating it. My background in architecture and design meant that I had quite a lot of experience in 2D and 3D modelling and rendering, and have my workflow of programs quite established and have a way of working I am happy with. I mainly use Rhino for all 2D and 3D work, and use the V-ray plugin to render within the Rhino environment. I like working this way as I am able to do everything in one program which saves a lot of time and Rhino is versatile program in which you can model at a really wide range of scales, from products to master planning.

However I was keen to explore some other 3D modelling programs this week and tried out Fusion 360 and Grasshopper through some YouTube classes and supplementary Fab Academy classes.

## Fusion 360

I was attracted to Fusion 360 because it can be used parametrically, which is really useful to make quick changes to a model without having to model the whole thing again. I followed a [<u>YouTube tutorial</u>](https://www.youtube.com/watch?v=A5bc9c3S12g) which was really useful to get going and learn some basic commands and modelling techniques. I really liked the environment and thought it was quite intuitive and felt very accurate and precise. One of the main aspects that I liked about Fusion 360 was the timeline feature along the bottom of the screen which makes it really easy to edit what you did and have it adapt to the rest of the model on the fly. Something which confused me a little at times was having to create a sketch every time I added a new element, but after a while I got used to it and it makes the model really tidy.

![]({{site.baseurl}}/fusion.jpg)

Fusion 360 differs from Rhino in that the faces are selectable and editable. This is nice and makes it easy to make sure you are building or subtracting from the correct part of the model. I also found this made me work more in the predefined views (ISO, front back etc) which was a different way of modelling for me.

![]({{site.baseurl}}/fusion2.jpg)

Another reason I liked Fusion 360 was the fact that you can add screws, and choose elements from actual manufacturers which makes the model feel real and adds a nice extra level of detail. This is done by going to Insert - Insert McMaster-Carr Component, and then choosing the component of your choice.

![]({{site.baseurl}}/fusion3.jpg)

## Grasshopper

A class gave by the tutor for the Fab Academy Barcelona, [<u>Eduardo</u>](https://iaac.net/dt_team/eduardo-chamorro/) showed us the possibilities of what Grasshopper is capable of, which is apparently anything and everything. I was surprised to learn that even machine learning can be applied to the environment to model and make predictions of different data sets. I did however find using Grasshopper really complicated and modelling in nodes felt really unnatural as I am used to having more of a sense of scale and control through being able to edit the 3D model directly. As a result I decided that Grasshopper wasn’t the program for me for now, but because it seems extremely powerful I will keep it in mind for future tasks and projects.

![]({{site.baseurl}}/grasshopper.jpg)

## Process & Final Project

For the documentation I thought it would make sense to talk about the process I went through to arrive at the final images I produced, for the final project and then talk about how each of the pieces of software contributed to the images. The final project I am proposing is a sequencer which controls different actuators to make music and sound, like an acoustic - physical, experimental drum machine whereby the sounds created come from custom made instruments and found objects. The idea is that different 'nodes' which contain different motors and solenoids will be connected together by serial so the number of inputs on the device does not limit the amount of instruments that can be controlled.

I would also like there to be compatibility with a computer via MIDI / OSC but this falls out of the scope of the Fab Academy and is more to do with the final project of my masters. One key reference for this project is [<u>the automate toolkit</u>](https://dadamachines.com/product/automat-toolkit-m/) created by [<u>dadamachine</u>](https://dadamachines.com/). Another key reference are the range of synthesizers and equipment produced by [<u>teenage engineering</u>](https://www.teenageengineering.com/). The model I created is almost like a precedent study as my project is aimed at people entering the world of electronics and music production and I think the aesthetic teenage engineering has developed is really suited to this context.

The general process started with some hand sketching and then translating this into 2D in Rhino. Once I had made the overall 2D drawing of my project, I exported the drawing into Illustrator to adjust the line weights and then exported the image as a jpeg for my website. For the 3D I used the 2D drawing I had made in Rhino as a starting point and then used V-ray for rendering. I then used Photoshop to edit the renders slightly, to clean up the image and add some details. The final images I have made this week are below, and then I will discuss how the different pieces of software I used contributed to these images.

{% figure caption: "*Line drawing showing sequencer with actuators and connection to laptop*" %}
![]({{site.baseurl}}/topline.png)
{% endfigure %}

{% figure caption: "*Render showing details of sequencer*" %}
![]({{site.baseurl}}/toprender.jpg)
{% endfigure %}

{% figure caption: "*Render showing sequencer with actuators and objects*" %}
![]({{site.baseurl}}/isorender.jpg)
{% endfigure %}

{% figure caption: "*Animation showing how units can be added*" %}
![]({{site.baseurl}}/gif-music.gif)
{% endfigure %}

## Rhino

As mentioned I already had experience using Rhino so felt confident modelling what I wanted my final project to be like. When I started using Rhino I used a series of tutorials from [<u>digital toolbox</u>](http://digitaltoolbox.info/) which were really helpful (there are also Grasshopper tutorials here).

![]({{site.baseurl}}/rhinoworkspace.jpg)

The workspace of Rhino is divided into different zones, like most 3D programs with. The main area (red box) can be divided into 4 zones (see image below), but by double clicking on the tab you can maximize the viewport as I have done with the top view. You can also toggle between the views on the panel shown in the green box, and even add additional views. To the right, the panel (blue box) shows the layer manager where there are other various settings such as saved views and properties (e.g. materials and lighting). Labelling and creating layers for the different elements in the model is really important and saves a lot of time in the long run, even when working in 2D.

The ‘snaps’ are highlighted in the orange box which are really important for drawing. Note the ‘project’ box which makes sure that you are drawing in the correct plane (true z axis for example) which is useful when drawing on surfaces, and if you want to draw planar curves. Finally, the pink box shows the command line. After using the program a lot you pick up the different commands and typing them in makes working a lot faster. Here are some of my most used and useful commands and tips I can think of beyond the basic move, rotate etc (there are of course many others):

* Make2D
* MakeHole
* PlanarSrf
* Scale1D
* Loft
* Sweep1
* ProjecttoCPlane
* Trim
* Join
* Hide / Show
* Zoom selected
* Export selected (from file menu)
* Explode
* Rotate 3D
* Extend
* BooleanSplit
* Spacebar to activate last used command

{% figure caption: "*Typical Rhino workspace*" %}
![]({{site.baseurl}}/rhinoworkspace2.jpg)
{% endfigure %}

For 3D modelling, I mainly use the gumball as opposed to typing in commands. There are always many ways of doing the same thing in CAD programs and this is the way that I have developed to work. I use it by typing in the distance I would like to move and then drag + cntrl to extrude a surface.

{% figure caption: "*Working from a 2D drawing and using the gumball to input figures*" %}
![]({{site.baseurl}}/gumball.jpg)
{% endfigure %}

I modelled the sequencer as well as some of the actuators and objects I would like to include in my project. I modelled most things myself from scratch, but there are numerous online free resources of existing models which can save a lot of time. I downloaded a solenoid and some glasses from [<u>Grabcad</u>](https://grabcad.com/library) and inserted the file into Rhino. The best way of doing this is to open a new Rhino file, drag the downloaded file onto the screen and then press insert / open file. This way you can edit the file more easily or add materials separately from the main model.

![]({{site.baseurl}}/insertfile.jpg)

In order to make the holes for the buttons and knobs on the sequencer I used the [<u>MakeHole</u>](http://docs.mcneel.com/rhino/5/help/en-us/commands/makehole.htm) command. With this command you can use a closed planar curve to make a hole or partial hole into a polysurface or surface, and is a command I end up using a lot. When using a new command it is useful to look at the command line as it gives you a step by step guide, or you watch a quick YouTube tutorial or look at the documentation on the [<u>McNeel website</u>](http://docs.mcneel.com/rhino/5/help/en-us/index.htm).

![]({{site.baseurl}}/makehole.jpg)

In order to make the buttons and knobs I used the gumball to extrude a surface and then the [<u>FilletEdge</u>](http://docs.mcneel.com/rhino/5/help/en-us/commands/filletedge.htm) to make them a bit rounded and a little more realistic. Using this command you just type it into the command line and then select the edge you would like to fillet and adjust the radius. Something nice about this command is that the number turns red if the number chosen is too big and it is impossible to form the geometry.

![]({{site.baseurl}}/fillet.jpg)

Once I had everything modelled I created a line drawing animation showing how different actuators and motors could be added to form a larger instrument setup. I created this by saving the desired view (I was in ISO mode) by going to 'named views' in the right hand side settings panel and clicking save, where you can name a view, which is especially important when rendering. I then used [<u>Make2D</u>](http://docs.mcneel.com/rhino/5/help/en-us/commands/make2d.htm) to create a line drawing from the 3D model. I then put each of the elements onto a different layer and then turned them on one by one, creating a line drawing of each one in the process. I then exported to Illustrator, adjusted line weights and then exported jpegs and created a gif with all of the images.

![]({{site.baseurl}}/gifprocess.jpg)

## V-Ray

I then created the renders of my final project. There are two important things to take into account when rendering which are lighting and materials. To render I did everything in Rhino using the V-Ray plugin which is nice to use and saves time not having to export to a different program. I first created a scene with some blank walls and floor to create a backdrop for the render, then added lighting which was a combination of a general light over the scene (rectangular) and a more direct light which are both found in the lighting menu. The properties and intensity are then adjusting in the properties menu, and it is always a process of trial and error to make the lighting the correct level, but as a rule of thumb I generally don't make rectangular lighting greater than an intensity of 10, and directional greater than 2 or 3.

![]({{site.baseurl}}/lighting.jpg)

I then added materials to the scene using the material editor within the V-Ray plugin and used a combination of pre-existing [<u>vismat materials</u>](https://vismats.com/) I had already downloaded and then some new ones I created for this series of renders. There are a number of ways to add a material to a surface and the way I normally use is selecting the material and right clicking the material as shown in the screenshot below:

![]({{site.baseurl}}/rendermaterials.jpg)

To effects to a standard material, you have to make a new layer which I did to create a glossy effect on the input / output connections on the side of the sequencer. To make a material glossy, this is done by adding a 'reflection' layer and adjusting the figure as shown in the screenshot below:

![]({{site.baseurl}}/renderstandard.jpg)

Once the materials and lighting are ready, I open the render option manager and run a test render at low quality to check the view and the general lighting settings. Once I am happy with this I run a series of higher quality renders in smaller regions to check how the different materials are rendering in order to save time in not rendering an image which has some problems with the material. I then run a higher quality render of the entire image. The final renders I produced this week only took about 20 minutes as they are just going to be viewed on screen and don't have to be really high quality for print.

{% figure caption: "*Rendering a test area to check materials and lighting*" %}
![]({{site.baseurl}}/renderregion.jpg)
{% endfigure %}

## Photoshop

After rendering I always put the images into Photoshop to edit slightly. For the top view render I added some numbers for example, and there are certain things you can do in Photoshop instead of rendering if you are short of time. I edited the colours of the knobs in Photoshop as it is a lot faster to do than do a load of test renders in V-Ray.

![]({{site.baseurl}}/photoshop.jpg)

## Illustrator

For editing the line drawings created in Rhino, I use the export selected option from the file menu, and export the lines as a dwg. I then open this file in illustrator which is a much better environment for producing line drawings as the interface gives you feedback on how the drawing will actually look. A trick I use to easily edit line weights is to create a different layer for each colour in Rhino for the different line weights I want, which are exported into Illustrator. In the case of the sequencer I wanted to buttons to be a lighter weight than the edge of the unit, so I made these a different colour and then used 'Select' - 'Same' - 'Stroke Colour' to select all the desired lines at once and then adjust all the line weights simultaneously.

![]({{site.baseurl}}/illustrator-week2.jpg)

## References

* [Fusion 360 Tutorial](https://www.youtube.com/watch?v=A5bc9c3S12g)
* [the automate toolkit](https://dadamachines.com/product/automat-toolkit-m/)
* [teenage engineering](https://www.teenageengineering.com/)
* [digital toolbox](http://digitaltoolbox.info/) Rhino & Grasshopper Tutorials
* [Grabcad](https://grabcad.com/library) free 3D models
* [Rhino McNeel](http://docs.mcneel.com/rhino/5/help/en-us/index.htm) documentation
* [Vismat](https://vismats.com/) materials for V-Ray
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week2/) (Computer Aided Design)

## Files

* [3D Model]({{site.baseurl}}/model-v2-render.zip) (3dm)
* [Illustrator Drawing]({{site.baseurl}}/line-drawing.zip) (dwg)
