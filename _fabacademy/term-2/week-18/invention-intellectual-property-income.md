---
title: Invention, Intellectual Property, and Income
period: 29 May 2019 - 5 June 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
The topic of the final Fab Academy class was associated with the next steps in bringing a project to life and into the real world. This means that themes such as intellectual property and how to generate income for a project were discussed which I found really useful as I am planning to continue the main project for my masters. The class coincided nicely with a business seminar called [<u>Emergent Business Models</u>](https://mdef.gitlab.io/oliver.juggins/reflections/emergent-business-models/) led by [<u>Ideas for Change</u>](https://www.ideasforchange.com/) so had already start to think about and engage with some of the issues that were brought up in the classes.

The assignment was to develop a plan for dissemination of the final project following an analysis of the theory covered in the classes. I will give an overview of some of the different topics and terms on touched on in the class, but will focus on the ones I found most interested and relevant for my project.

## Key Concepts

The class this week was led by Oscar, Guillem and Viktor of the [<u>Smart Citizen</u>](https://smartcitizen.me/) team in the Fab Lab Barcelona. The [<u>documentation</u>](https://hackmd.io/s/HJZ9KGRoE) and notes for the class gave an overview of the different types of licence and terms associated with invention, intellectual property, and income. The subject matter was all quite new for me so I learnt a lot about and have been able to form a strategy for moving forward with my project.

Oscar explained that the rights in copyright operate on a scale, from the being totally open and in the public domain (all rights relinquished) to a trade secret (all rights retained). An example of a trade secret is the recipe for Coca-Cola for example, as it is information that they protect.

Patents was one of the topics covered that I already had quite a good idea and understanding about before the class. It is a form of intellectual property which enables the owner of the patent to control the legal right over their invention to prevent others from selling, using and making it for a limited period and location in some cases. For my project I am not very interested in the idea of a patent, and think it is not very relevant for me. The way it was presented in class made it seem like it is a lot of responsibility and effort for the person or organisation owning a patent as you are responsible if someone copies your invention.

## Types of License

An overview of the types of licences was given, here is a summary and some thoughts about the ones that interested me the most:

<u>GNU General Purpose License</u>

A free software license which gives permission for others to share and modify the software, with the belief that there should be no restrictions and that you should be able to change it for your own needs, and to be able to share these changes.

A well-known example is the operating system Linux Kernel. In the case of Linux, choosing to be open has been extremely beneficial, as it meant that people were able to contribute to its development through offering suggestions on how to improve it. Through having this open model, both parties benefit as the product increases in quality for the creator, and a sense of empowerment for the user is created as they have actively played a role in making it better.

The animation and 3D software [<u>Blender</u>](https://www.blender.org/) is another similar example. Viktor from the Smart Citizen team had given some classes throughout the year on this software and in a discussion after class told me more about the type of [<u>license</u>](https://www.blender.org/about/license/) it has (GNU General Public License) and why he likes it so much. He talked about how he felt part of a community, and that he has contributed to its development.

With this type of licence, my understanding is that because it is totally open it is totally legal to sell this free software. In the case of Blender I think this has happened to unsuspecting individuals who were not aware that the software is in fact totally free.

<u>Copyleft & FOSS</u>

Copyleft was a term I had heard but not fully understood, and is a way of making work or a program free to use but means that all subsequent versions of the program must also be free. FOSS ('free and open source') is a term that describes software that is both free and open source and allows the user to see the source code. Copyleft licences are open source, and free but not all licences that are free and open source are Copyleft.

It therefore differs from putting work in the public domain, which is simpler but means that people can then make any sort of change and then distribute it as a proprietary product.

<u>Creative Commons Licenses</u>

There are many types of Creative Commons licenses, and is quite complex with lots of different permutations. As is the case with making a choice of license for your project, you have to ask yourself questions about what you want your project to do, and what you believe in. I did some more reading on the types of [<u>Creative Commons licences</u>](https://creativecommons.org/share-your-work/licensing-types-examples) to try and understand it a little better. There are four conditions, and six Creative Commons licenses based on these conditions which are as follows:

 * Attribution (by)
 * ShareAlike (sa)
 * NonCommercial (nc)
 * NoDerivatives (nd)

 The types of licenses are formed by combining these conditions:

* Attribution (CC BY)
* Attribution ShareAlike (CC BY-SA)
* Attribution-NoDerivs (CC BY-ND)
* Attribution-NonCommercial (CC BY-NC)
* Attribution-NonCommercial-ShareAlike (CC BY-NC-SA)
* Attribution-NonCommercial-NoDerivs (CC BY-NC-ND)

## Insights & Discussion

Following the overview was an open discussion where Oscar, Guillem and Viktor and other members of the Fab Lab shared their views and experiences. They could share their views and experiences from working on the Smart Citizen project, which is licensed under a Creative Commons Attribution-NonCommercial ShareAlike 4.0 International License.

The discussion was mainly related the context of software but can relate to any kind of content and gave me lots to think about as I am producing content which I want people to have access to and share. It was really reassuring to listen to them and acted as proof that it is possible to make money and be open, and is a way of thinking that I have become a lot more interested in and believe in, being around the community of the Fab Lab, the Fab Academy MDEF and IAAC.

There were lots of ethical and moral questions that were touched on and something that was said that stuck with me was having to make the choice that you believe will help create the change you want to see. The whole spirit of the Fab Lab and MDEF is based around sharing knowledge, and trying to tackle complex problems that are prevalent in society today. Complex problems can't be solved be individuals, so opening up knowledge is the best way to do this.

## Chosen License

As part of the MDEF programme however the suggested license is as follows:

*"The copyright license regulation for the projects developed in the MDEF program inherent to
the Fab Academy program will follow the regulation of the Fab Academy Program:
Default license for student's work will be under Creative Commons: Attribution-ShareAlike
4.0 International (CC BY-SA 4.0)
Students interested in researching further can change their licensing if expressly mentioned."*

Having done some research and being generally more informed from the classes I wanted to make a decision independently as well. I am trying to make machine learning available and accessible for children to learn about so it doesn't make sense to close this knowledge, so the choice is quite straightforward.

I followed the flow diagram exercise on the class [<u>documentation</u>](https://hackmd.io/s/HJZ9KGRoE) and the license I ended up with was an Attribution-NonCommercial-ShareAlike (CC BY-NC-SA).

![]({{site.baseurl}}/cc-logo.png)

Click [<u>here</u>](http://creativecommons.org.au/content/licensing-flowchart.pdf) to try it for your own project and have a look at it here:

{% figure caption: "*Source: creativecommons.org.au/*" %}
![]({{site.baseurl}}/cc-license-flowchart.jpg)
{% endfigure %}

## Proposed Plan for Dissemination

<u>Ideas for Change</u>

The plan I have proposed for the dissemination of my project has been developed taking into the consideration the knowledge and concepts gained from the [<u>Emergent Business Models</u>](https://mdef.gitlab.io/oliver.juggins/reflections/emergent-business-models/) module that formed part of the masters. The course was led by Javier Creus and Valeria Righi of [<u>Ideas for Change</u>](https://www.ideasforchange.com/) and the [<u>pentagrowth model</u>](https://www.ideasforchange.com/pentagrowth) they developed formed a large part of the learnings in the seminar, and my business plan as a result.

To summarise the model identifies five ‘exponential growth levers’ that together ‘brings a new point of view on the keys that can foster the growth of organizations in the digital environment by combining your internal assets with those available in your business ecosystem’ (Ideas For Change, n.d.). The model came as a result of analysis carried out by Ideas for Change on 50 organisations which have grown more than 50% in the number of users and in terms of revenue for 5 consecutive years in a period from 2008. Some of these organisations include the likes of Netflix, Dropbox and Arduino. These growth levers are shown in the following diagram:

![]({{site.baseurl}}/pentagrowth-model.jpeg)

<u>Proposed Business Plan</u>

A business I have created for the first 2 years to develop and disseminate the project are as follows:

* Create series of workshops teaching children about machine learning and test in Barcelona and the UK.
* Develop a range of 'kits' to accompany the workshops.
* Create website and GitLab for project to include documentation on activities and workshops.
* Launch online platform for project for sharing of knowledge and learnings that others can contribute to.
* Through workshops and the online platform, the aim is a community around the project will start to form.
* Connect and collaborate with others working in the digital literacy field in Barcelona and the UK.
* Develop a 'package' for schools to choose from including a range of activities and products to choose from.

<u>Income</u>

Because the content I produce will be open for people to use and remix, I will have to find another way to generate income for the project. I will then focus on the service that the project offers, which is a common strategy in open source projects. The service will include the design and development of workshops that incorporate machine learning into the curriculum for schools.

Another way income could be generated is through donations. This is not something I would like to rely on, but there are projects that successfully generate this money. From Viktor I learnt that Blender generates enough money to sustain itself. At this time of writing, (June 2019) Blender receives [<u>€ 34007 / month</u>](https://fund.blender.org/) through its development fund. They have a [<u>subscription</u>](https://www.blender.org/foundation/donation-payment/) service where you can donate.

In order to make people willing to donate though, they have to be happy with the service provided and I think that feeling part of a community around the product is also important, so you have a sense of where your money is going and how you are helping to develop the project.

## Reflection

This week provided a lot of food for thought and really learnt a lot from the class given in the Fab Lab which demystified a lot of terms and made me think about the ways I can continue my project in a way to both share knowledge and make some money. I have been inspired by the community that the Fab Lab and projects around it are a part of and my experiences during the year as part of MDEF and being in this environment have shaped my views on this topic considerably and the futures plans I have for my project.

## Useful links

* [Emergent Business Models](https://mdef.gitlab.io/oliver.juggins/reflections/emergent-business-models/) seminar documentation
* [Ideas for Change](https://www.ideasforchange.com/)
* [Smart Citizen](https://smartcitizen.me/)
* [Permissive vs Copyleft Licenses](https://opensource.stackexchange.com/questions/21/whats-the-difference-between-permissive-and-copyleft-licenses)
* [Blender License](https://www.blender.org/about/license/)
* [Creative Commons Licenses Overview](https://creativecommons.org/share-your-work/licensing-types-examples)
* [Creative Commons FLowchart](http://creativecommons.org.au/content/licensing-flowchart.pdf)
* [Pentagrowth model](https://www.ideasforchange.com/pentagrowth)
* [Invention, Intellectual Property, and Income](https://hackmd.io/s/HJZ9KGRoE) Class Documentation
* [Fab Academy BCN Documentation](http://fabacademy.org/2019/labs/barcelona/local/wa/week18/) (Invention, Intellectual Property, and Income)
