---
title: Computer-Controlled Cutting
period: 06-13 February 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
The assignment in this week of the Fab Academy was to explore the computer controlled equipment in the Fab Lab which include the laser cutter and the vinyl cutter. This week was the introduction of the group assignment which was to test the laser cutter and try the equipment with some different settings.

## Vinyl Cutting

For the vinyl cutting task I decided to make a sticker of a company I used to work for, [<u>Tu Taller Design</u>](https://www.tutallerdesign.com/). I had an esp file already on my computer, but I had to edit it slightly to make it suitable for the cutter. This is what the original file looked like:

![]({{site.baseurl}}/tutaller.jpg)

To edit the file I had to ungroup everything and then change the parts which were solid to lines, so that the design consisted only of lines, as this is how the vinyl cutter works, following the paths of single lines. Once the file was all in single lines, I made the thickness of the lines to 0.25 in illustrator and exported as a dxf ready to be used with the software for the vinyl cutter.

![]({{site.baseurl}}/tutallerfinal.jpg)

I used the [<u>Silhouette Cameo</u>](https://www.silhouetteamerica.com/shop/cameo/SILHOUETTE-CAMEO-3-4T) machine to cut my sticker which was a really nice machine to use although I had some difficulty at first. Once the file is ready in the software you have to load the material by lifting up a flap on the printer and then securing the material again. You then send a test cut which worked fine for me, but when I sent the sticker to be cut the material didn’t stay stable and the letters in the design became skewed.

![]({{site.baseurl}}/vinylwrong.jpg)

I got some more material, realigned the material and secured it properly and my design cut successfully which I then transferred to my notebook. This is what the material looks like when secured correctly (note the highlighted lever for securing the material):

![]({{site.baseurl}}/cuttingmachine.jpg)

![]({{site.baseurl}}/sticker.jpg)

## Group assignment

For the group assignment we attempted a test cut on the laser cutting for a press fit joint, raster and engraving. For the press fit test we used 6.5mm cardboard and made a series of notches that increased by 0.5mm from 6.25mm to 6.75mm. Unfortunately the settings were incorrect for the cutting, engraving and raster resulting in the cardboard looking like this:

![]({{site.baseurl}}/group.jpg)

Despite the exercise not having the desired effect, I still learnt from the experience and carried this knowledge into my individual task which had more success.

## Parametric Design

The design for the press fit laser cutting task was inspired by a piece from the [<u>automat toolkit</u>](https://dadamachines.com/product/automat-toolkit-m/) by [<u>dadamachines</u>](https://dadamachines.com/). The idea is to have a structure that can be put together in different ways that could serve as a drum of some sort when hit by a servo motor with a drum stick added which could become part of my final project.

![]({{site.baseurl}}/dada.jpg)

I designed the structure in Rhino and then transferred the model to Fusion in order to make it parametric so I could easily adjust the model depending on the material available. As part of the design I had to be aware of the [<u>kerf</u>](http://www.cutlasercut.com/resources/tips-and-advice/what-is-laser-kerf) which is the term used to describe the material which is lost to the laser burning the material. This value differs between materials and has to be taken into account when designing the file, especially in a press fit model task as the pieces have to fit together nicely without falling apart.

![]({{site.baseurl}}/fusion.jpg)

Once the file was ready in Fusion I exported a dwg to be put back into Rhino so I could lay out and arrange the pieces to be laser cut. When laying out the convention is to use different colours for different purposes (cut, engrave and raster). I had to make the lines I wanted to be engraved red, blue for cutting and then black for rastering.

![]({{site.baseurl}}/rhinonest.jpg)

To take into account the kerf and material tolerance I offset the holes in the design by 0.1mm, which in the end worked out as the structure could be put together in different configurations without falling apart, and fulfil the ‘press fit’ requirement of the week. The size of the material I had was 690mm x 600mm 4mm plywood and I nested it with Gabor’s design as we were sharing material.

![]({{site.baseurl}}/nestedreal.jpg)

## Laser Cutting

I had used the laser cutter in the Fab Lab a couple of times, but this week was a good chance to get some more independence on the machine and now feel I could operate it without any assistance. Once the file is ready in Rhino there are a series of steps to take in order to print successfully, but before any of that, make sure the extraction is turned on in the Fab Lab laser cutting space. The machine we used had a bed size of 1000mm x 600mm and was the [<u>Trotec Speedy 400</u>](https://www.troteclaser.com/en/laser-machines/laser-engravers-speedy-series/).

![]({{site.baseurl}}/machine.jpg)

First you have to send a test print in order to check if the settings are cutting the material properly. This is normally done by drawing a small circle where you have space on your material and checking the settings. Once this circle is ready, you send to print, check the setting is ‘vector’ and the correct printer is selected and press print. Also make sure ‘minimize to job site’ is unchecked so that the cuts happen in the correct location. The job should then appear in the Job Control program. Now you can remove any existing jobs and check in the preview if everything looks ok. If it doesn’t then you might have to go back into Rhino and send the print again.

You are able to send cuts and engravings at the same time, but rasters have to be sent separately. The difference is in Rhino you have to select ‘raster’ not vector in the print settings. In both cases however, you will have to adjust the power, speed and frequency settings depending on the material being used. The settings we used for 4mm plywood were:

* Power: 68
* Speed: 100
* Frequency: 1000

We worked this out by using the samples with suggested figures and going in between, and increasing the power slightly due to the test print. The final step is to focus the laser, which is done with the guide you hang off the machine’s laser body and increasing the height of the bed until it falls off. Just before printing as well you can select the option ‘optimize vector’ to make the whole print job quicker so that the laser takes the most efficient path for the design.

![]({{site.baseurl}}/samples.jpg)

In the end, the cutting was successful for the most part, but the board not sitting entirely flat on the surface of the bed meant that not all cuts when through the board and I had to use a craft knife to get out the rest of the pieces, which was a bit frustrating but next time I will know to make sure the piece of material is flat on the bed. The pieces stuck together well, and can be configured in the ways as shown below. The idea is that in future weeks of the Fab Academy I will be able to build the necessary parts to build a drum powered by a motor or a solenoid that will move to hit the structure to make sound.

![]({{site.baseurl}}/c.jpg)

![]({{site.baseurl}}/b.jpg)

![]({{site.baseurl}}/a.jpg)

## Useful links & References

* [Tu Taller Design](https://www.tutallerdesign.com/)
* [Dada Machines Automat Toolkit](https://dadamachines.com/product/automat-toolkit-m/)
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week3/) (Computer Controlled Cutting)

## Files

* [Vinyl Cutting]({{site.baseurl}}/vinyl.zip)
* [Laser Cutting]({{site.baseurl}}/laser.zip)
