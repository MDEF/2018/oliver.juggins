---
title: Electronics Design
period: 27 February-06 March 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
<!-- Understand what the different parts of the circuit are doing.
Need to add a button and an LED to the board. Assignment - make board do something.
Redraw the board - start from scratch. Place each part, and add LED and button, and fabricate it. The test it. Then use Neil's program to test the board it working.
Goal of assignment is to get going with the design tools.
The use equipment in the lab to test the board.
Also talk about the failures at the end, what went wrong (missing parts in the Eagle, missing parts.
Include files

## Reflection

Made a lot of progress with using Eagle.
Soldering still find difficult.
Progamming the board still hard-->

This week of the Fab Academy continued the work done in week 4 ([<u>Electronics Production</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/electronics-production/)) where the requirement was to not only produce a PCB but also design it. As a result I already had some experience in the milling and soldering aspects of the task, but designing a PCB was totally new to me. The learning curve was a steep on again, but a week where my knowledge of electronics progressed a lot. The task was to redraw the echo [<u>hello-world</u>](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.png) board from scratch and add at least a button and an LED (with current-limiting resistor), checking the design rules, make it and then test it.

## Basic Electronics

As part of the Fab Academy classes given in Barcelona there was an introduction to the basics of electronics which covered what the different components were and their function. These were really useful as my prior electronics knowledge was a result of doing small scale projects with Arduinos so hadn't had a chance to properly get into some of the fundamental principles of electrical engineering. When using the Arduino I found [<u>this video</u>](https://www.youtube.com/watch?v=abWCy_aOSwY) from Jeremy Blum quite useful and I also found [<u>this explanation</u>](https://www.youtube.com/watch?v=6Maq5IyHSuc) which covers the different components in more detail.

The documentation on the [<u>Barcelona Fab Academy 2019</u>](http://fab.academany.org/2019/labs/barcelona/local/wa/week6/#references-and-resources) for electronics design is extensive and was also really useful for many aspects of the assignment so would recommend this as a valuable learning resource too.

## Designing the Board

Designing the board required the use of some software to lay out the components and prepare a file which could be sent to the milling machine. I used [<u>Eagle</u>](https://www.autodesk.com/products/eagle/overview) from Autodesk as we had a tutorial in class and I have used a number of their products before and think their software is generally very good.

As I am essentially a total beginner with electronics and had not used Eagle before this week, I decided to keep things simple and fulfil the minimum requirements of adding an LED and resistor to the hello world board.

In a tutorial in class we went through the process of adding the fab library to Eagle and placing components in the Schematic. When adding components you have the add labels in order to connect the different components. These were the components that were added:

* 6-pin programming header
* Microcontroller: ATtiny44A
* 20MHz crystal
* FTDI header
* Resistors (x4 0 ohm resistor the jumper in my case, 499 ohm resistor for the LED and 10K ohm resistor for the button)
* Capacitors 2 x 22 pf capacitors
* Ground
* VCC
* Button
* LED

![]({{site.baseurl}}/naming.jpg)

Once all of the components are connected in the schematic you can move to the 'Board' window in order to start laying out the actual design on the board and how it will look in reality, which makes it a lot more tangible as you can see the shape of the final board more clearly.

However when arriving at this task all of the components are connected by yellow lines which you have to try and untangle by moving the different components around the screen. Using the 'ratsnest' command every once in a while helps to untangle the lines a bit. This can be a lengthy and frustrating process, but nowhere near as lengthy and frustrating as when connecting the components for real with the 'route' tool. To untangle the yellow lines as much as possible I used the 'rotate' tool but right clicking the mouse.

When the lines were as neat as possible I started connecting the components using the line thickness of 16mm as this was the recommended figure to use. At first I didn't have much of a thought through methodology to place the components and place and went through a process of trial and error to end up with the with the first of many iterations.

![]({{site.baseurl}}/eagleinitial.jpg)

As I was just getting used to the software and playing around I started over and tried redrawing the board many times to try and develop my skills. This is another iteration:

![]({{site.baseurl}}/firstiteration.jpg)

I realised that my approach at this stage wasn't really producing the desired results so I tried to be a little more structured in connecting the components and used the boards of other students in the Fab Academy archive as a guide for the layout of the components. I then arrived at this board design:

![]({{site.baseurl}}/failedattempt.jpg)

I neatened up the file to make it more presentable as a PCB and prepared the file for milling, and cut the board:

![]({{site.baseurl}}/failedcutting.jpg)

There were a number of problems with this outcome, despite the quality of the milling to actually be quite good, and much better than my FabISP board. First of all there were some key connections and components missing from the board (FTDI header missing two connections and a resistor was also missing). I intentionally didn't cut the board out as it was during the process of milling the board I realised there were some errors, but I still managed to send the outline in the traces file anyway.

I then returned to the schematic and added all of the components and connections and got some classmates to check it to make sure I didn't waste time milling an incorrect board again.

![]({{site.baseurl}}/finalschematic.jpg)

I was then met with this in the 'Board' window.

![]({{site.baseurl}}/allcomponents.jpg)

After a lengthy process of moving around and connecting components I finally was able to connect everything successfully. I did however use a couple of extra resistors as 'jumpers' in order to connect everything. It was during this timeframe where I began to feel a lot more confident using Eagle.

![]({{site.baseurl}}/readyrough.jpg)

I then tried to make the design neater and more efficient to be milled.

![]({{site.baseurl}}/ready.jpg)

At this stage the file needs to be checked using the design rules. I downloaded the required Fab Academy [<u>design rules file</u>](https://gitlab.fabcloud.org/pub/projects/blob/master/files/fabmodule.dru) file to do this. I encountered some problems loading this file in Eagle however so copied the code from GitLab and 'saved as' a .dru file which solved the problem.

In order to do carry out the design rules check go to tools > DRC and load the fab design rules final and hit 'check'. When I did this a load of errors appeared including 'Airwire' and 'Overlap'.

![]({{site.baseurl}}/drc.jpg)

These errors were mainly due to the jumper resistors I used and the other errors were easy to correct by doing some simple redrawing. I then could make the size of the board a bit smaller and then get the file ready for exporting as an image to be milled. In order to export the image I turned of all of the layers apart from the traces, and exported as a png, setting the file quality to 1000 dpi and making sure monochrome was checked.

![]({{site.baseurl}}/finaleagle.jpg)

I then reopened the png in illustrator in order to draw the outline of the board, as someone in class had problems using the outline that was exported from Eagle. I used a 3pt line and exported the file at 1000 dpi also.

![]({{site.baseurl}}/illustrator.jpg)

## Milling the Board

I then prepared the files in [<u>Fab Modules</u>](http://fabmodules.org/) to get the file ready for milling which was straightforward as I had done this during [<u>Electronics Production</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/electronics-production/) two weeks previously.

![]({{site.baseurl}}/fabmodules.jpg)

I then set up the machine with my 1/64" end mill, set up the machine with the x,y and z coordinates using the Roland SRM-20 in the lab and sent the file to be milled.

![]({{site.baseurl}}/millingfinalboard.jpg)

I was left with this result for the traces which I was happy with:

![]({{site.baseurl}}/traces.jpg)

I then sent the outline to cut the board out and was left with this as the result:

![]({{site.baseurl}}/finalboardbare.jpg)

By this stage I was happy to have cut a board given my previous attempt was incorrect, but next time I would make some changes. I would make the board a bit smaller and try to control the 'negative' space a bit more on the board to make it easier to solder.

## Soldering the Board

Before I started soldering, I had to collect the correct components from the electronics lab. I wrote the required items down and stuck them in my sketchbook, like I did in electronics production week.

![]({{site.baseurl}}/sketchbook.jpg)

I realised I was missing some components once I started soldering which were the 22pf  capacitors so returned and got them. I used my schematic and board layout on Eagle as a guide for the components and tried to follow a similar methodology as when I soldered the FabISP board by starting with the ATtiny microcontroller. I then started working my way around the board in a logical manner trying to make the process as easy as possible. This time around I found the board a lot easier to solder, I think the fact the board was a little bit bigger helped. I had my board and soldering check by one of the instructors, Eduardo who gave me some really useful tips. I had not used enough solder in a lot of places for the components and my technique was lacking. He showed me a four step process of how to solder which is as follows:

* Apply soldering iron to the board.
* Apply solder to the board, and wait for a large blob to appear.
* Remove solder from the board.
* Remove soldering iron from the board.

I applying this technique to the components that were lacking enough solder helped a lot, but for the smaller components such as the microcontroller I needed to be a lot quicker that with the 6 pin header for example.

![]({{site.baseurl}}/solderedboard.jpg)

## Programming the board

Just like in [<u>Electronics Production</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/electronics-production/), once the board is soldered it needs to be programmed. The task was to make the button turn on/off the LED but the difference this week was that the FabISP would be used to program the board produced this week. I had less problems with the soldering with this week's board so a little re-soldering was required but this process was fairly quick and straightforward. I used [<u>this tutorial</u>](http://highlowtech.org/?p=1695) to program the board using the FabISP and the [<u>documentation</u>](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html) produced by the Fab Academy.

First of all I had to connect the two boards using some wires and clips that connect the 6 pin headers together.

![]({{site.baseurl}}/programming.jpg)

The board is programmed through using the Arduino IDE environment using Arduino code. Something to keep in mind with this code however is that the pinouts on the ATtiny 44A microcontroller are not the same number in the Arduino code. To use the FabISP board within the Arduino IDE it is necessary to download the correct library in order to use it, which can be found [<u>here</u>](https://github.com/damellis/attiny). I followed the tutorial once the library was installed but once I uploaded the code I received an error when 'burning to bootloader'.

![]({{site.baseurl}}/programmingerror.png)

After some debugging I realised that although I had set up the board, port and programmer correctly in the tools menu I had not selected the correct processor or clock.

![]({{site.baseurl}}/toolsnotsetupcorrectly.png)

I then did not have any problems burning to bootloader and could program the board using the code from the tutorial to make the LED turn on and off with the button.

![]({{site.baseurl}}/bootloadersuccess.png)

Within the code I had to find the part which defined the pin numbers in the 'blink' example and change the 'buttonPin' from 2 to 3 and ledPin from 13 to 7 due to the difference in pinouts between the ATtiny microcontroller and the Arduino.

<!-- ![]({{site.baseurl}}/gifchange to have hand.gif) -->

## Useful links

* [Basic Electronics](https://www.youtube.com/watch?v=abWCy_aOSwY)
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week6/) (Electronics Design)

## Files

* [Schematic]({{site.baseurl}}/electronics-design-schematic-oj.sch) (Eagle)
* [Board]({{site.baseurl}}/electronics-design-board-oj.brd) (Eagle)
* [Traces]({{site.baseurl}}/traces-final-oj.png) (png)
* [Interior]({{site.baseurl}}/interior-final-oj.png) (png)
* [Traces]({{site.baseurl}}/traces-final-oj.rml) (rml)
* [Interior]({{site.baseurl}}/interior-final-oj.rml) (rml)
