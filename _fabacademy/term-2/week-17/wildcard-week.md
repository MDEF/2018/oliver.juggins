---
title: Wildcard Week
period: 22-29 May 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
Wildcard week in the Fab Academy is a week to experiment with a new type of fabrication method and technique that has not been covered in the course of the classes. Some different examples and mediums to work with include composites, robotics, textiles, cooking. In the lab there were classes on wood bending, clay printing with the KUKA Robot and welding. The assignment was to document the experience of one of these classes, and I chose to do welding. There was a group of 6 of us that did the class together, led by Mikel the Fab Lab manager.

## Welding 101

There are many types of welding, all with the shared goal of connecting pieces of metal, but the most well known and widely used are MIG, TIG and Stick (also known as Arc) welding. The selection of the type of welding that you would like to use depends om the type of project you are doing and each have their advantages and disadvantages. This table gives an overview of the different types and some of the different qualities of each:

{% figure caption: "*Types of welding. Source: Kings of Welding*" %}
![]({{site.baseurl}}/welding-table-types.jpg)
{% endfigure %}

As a general overview, MIG welding is a good entry to welding as it is relatively easy to learn, is a cheaper option than some other types of welding and has a good quality of weld to price ratio. TIG welding on the other hand is a bit more difficult to learn but produces a higher quality weld and all in all takes longer. Stick or Arc welding is normally used to cast steel and iron and like MIG welding is quite straightforward to learn. It has the advantage of being more portable and can be used outdoors but produces a lower quality weld.

## MIG welding

In the lab the type of welding that we learnt about and had a go at is MIG welding. MIG welding stands for 'metal inert gas', because gas is used in this welding process. The process works by joining metals together through heating them up with an electrical arc between the metal you are working with and a wire electrode which is fed through a 'gun'.

A 'shielding' gas is also released through the gun in order to create a zone to stop the arc and material being welding or the 'weld pool' from being contaminated. A contaminated weld pool means that Oxygen and Nitrogen will interact with the weld and could become porous.

A clamp needs to be connected to the workpiece to earth the material. This diagram contextualises these terms and gives an overview of the MIG welding process:

{% figure caption: "*MIG Welding visual description. Source: Kings of Welding*" %}
![]({{site.baseurl}}/migweldingsetup.jpg)
{% endfigure %}

This type of welding is good because as mentioned it is easy to learn, is cheap and produces good quality welds. The gas released by the gun means that 'slag' isn't produced meaning less cleaning up. Slag is the term used to refer to the residue left over from welding. Another advantage of MIG welding is that you have both hands available, unlike Stick welding so you can have more control over your weld.

![]({{site.baseurl}}/gas-weld.jpg)

MIG welding requires some setup in order to obtain a good quality weld. There is a setting on the generator and another on the gun itself which set the voltage and speed at which material is released respectively. The voltage and speed set depends on the type of job you are doing, and your ability as if you are welding very think material you want to probably weld quickly so that you don't risk overheating the material and burning a hole.

In general the two most popular gases for MIG welding are Carbon Dioxide and Argon, or a mixture of both. A mixture produces the highest quality weld and is what is used for in the setup in the lab (85% Argon and 15% CO2). This combination is preferable as the 'arc stream' is a better size, resulting in a higher quality weld.

The gas used in the lab is called Argon 15 (85% Ara)
The other settings we used in the lab were as follows:

![]({{site.baseurl}}/welding-settings.jpg)

Another aspect of the MIG welding process that affects the quality of the weld is the distance at which the metal sticks out of the gun or the 'electrode stickout'. The electrode should stick out about one centimetre from the nozzle. The shorter the electrode is, the hotter the weld will be and if it is too longer there will be splatter which can stick to the end of the electrode. This should be cut off when this happens as gas flow can be reduced to the weld pool and reduce weld quality through porosity.

## Ready!

Safety is very important to be mindful of when welding. There are many sources of danger and opportunities to cause harm so here is a checklist of things to be aware of that I picked up during the class:

* When cutting the metal for use, always use gloves as the sharp edges can cut your hand.
* These sharp edges should then be grinded to give them round edges to avoid cutting someone.
* When welding use the larger fabric gloves (the rubber ones can melt).
* A welding jacket or overalls should be worn (flame resistant).
* Clothing in general should cover as much skin as possible as the sparks generated can cause burns and should be flame resistant.
* A good level of protection for footwear should be worn, high top boots and leather shoes are ideal.
* Helmets are required with the right level of shade to protect the eyes as arc radiation can cause a lot of damage.
* When grinding the metal glasses should be worn to protect eyes from sparks and debris.
* Due to location of MIG welding setup in the lab there was a flame resistant curtain that should be drawn so that people on in the Fab Lab don't see the sparks and radiation produced.
* Workspace should be clear so there is nothing to trip over, this is especially true for cables.
* Work area should be dry, as water conducts electricity so should check surroundings before starting to weld.
* When the person welding is preparing to weld they should shout 'ready!' so that everyone puts their mask down to protect their eyes. You should only start welding once everyone has replied and properly prepared.

![]({{site.baseurl}}/types-of-helmet.jpg)

There were two types of helmet in the lab, one which was always dark and produced a spark when you start welding and another which you could always see through and responded to the type of arc produced. The latter is a lot better and easier to weld with so we used those ones when welding.

## Demo - Backhand Welding

Mikel gave a demo of how to weld and the process that we should go through. He first cut metal down to size using the cutter.

![]({{site.baseurl}}/cutting-metal-weld.jpg)

He then did some tests with the voltage and release speed of the electrode and created some lines using the 'backhand / pulling' method. This is where you pull the nozzle towards your body.

![]({{site.baseurl}}/mikel-demo.jpg)
Here is a video of Mikel in action:
<br>
<br>
<iframe src="https://player.vimeo.com/video/339725995" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

## Butt Joint

Each of us tried out hand at a butt joint, which is a relatively simple horizontal weld to connect two pieces of metal perfect for beginners. All that is required is that you weld down the joint of the metal, and try and produce a continuous, thick bead. For this type of joint the process is to weld on both sides and then grind down the bead so that the piece of metal is flat.

When it was my turn the metal was prepared, the workspace was clean and the metal was clamped. By this time a couple of people had already been so the settings on the welder had been fine tuned.

![]({{site.baseurl}}/clamping-weld.jpg)

![]({{site.baseurl}}/action-weld.jpg)

![]({{site.baseurl}}/welding-sample.jpg)

The sound the weld I did produced sounded good, with not too much splatter and I tried to move the little circles and side to side to make the bead thicker. My weld is on the left hand side and am quite happy with the result. The section at the end (bottom left of pieces of metal) is the highest quality weld. Emily had gone before me and grinded down her weld, which was in the centre of the piece of metal. Because there are no holes in it you can tell it is of a high quality. Her attempt at a circle was pretty impressive too!

I looked at this [<u>tutorial</u>](https://www.instructables.com/lesson/Welding-Joints/) after the class to learn a bit more about the technique of welding. When using a thicker metal you can grind down the edges of each piece of metal and make a groove or 'v joint' so there is more surface area for the bead to fit it. We were welding a 'square joint' as our piece of metal was quite thin.

## Corner Joint

Next I tired to do a corner joint, which was a lot harder. A corner joint is connecting two pieces of metal at a right angle to form an 'L' shape. With thin metal corner joints are especially hard as it is easier to make the metal can overheat and make a hole. I just tried welding the joint on the inside, but you can weld over it on the outside too to make it stronger.

The process consisted of using a guide to keep the metal at a right angle and then welding some 'dots' to connect the metal so it would stand uprights without the guide. I then connected the dots, using the same technique as with the butt joint, but was not able to achieve a bead of as high a quality.

![]({{site.baseurl}}/join-weld.jpg)

Here is an example of what a corner joint should look like:

{% figure caption: "*Types of welding. Source: Instructables*" %}
![]({{site.baseurl}}/corner-joint-example-table.jpg)
{% endfigure %}

## Final reflections

The welding class was really enjoyable and is definitely something I would like to try more of. I would like to try more types of joints and make something big such as a part of a table. The next step could be to incorporate digital fabrication through laser cutting metal and connecting it or combining is with some different materials and digital fabrication methods. An idea was to use welded metal as the joints with timber to create a table like this example:

![]({{site.baseurl}}/metal-joint-example-table.jpg)

## Useful links

* [MIG Welding Guide](http://www.kingsofwelding.com/guides/mig-welding-guide/) (Kings of Welding)
* [Welding Joints and Beyond](https://www.instructables.com/lesson/Welding-Joints/) Instructables Tutorial
* [Fab Academy BCN Documentation](http://fabacademy.org/2019/labs/barcelona/local/wa/week17/) (Wildcard Week)
