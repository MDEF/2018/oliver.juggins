---
title: Embedded Programming
period: 13-20 March 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
<!--Include all code. Try and program in different ways...?-->

This week of the Fab Academy was a continuation of the electronics-oriented tasks of Fab Academy, but programming side of things. My background in programming was very limited before starting the Fab Academy so this week was a good chance to learn more about this world and develop my skills. The assignment was to read a microcontroller data sheet and to program my board to do something suing as many different programming environments as possible.

## Reading a Data Sheet

Reading the [<u>data sheet</u>](http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf) was the first task of the assignment and is an important step in better understanding the AVR Microcontrollers. This was helpful as it made the work carried out in [<u>week 6</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/electronics-design/) a lot more sense. For example, the pin descriptions made me understand what each pin was actually doing (before I was only really aware of ground and VCC). This is a diagram showing the different pins on the ATtiny 44:

![]({{site.baseurl}}/attiny44-pins.png)

I turned to the Fab Academy tutorials for the embedded programming week in order to understand some of the terms in the data sheet better, and used them to decide the way I would program my board:

* [Makefile for Embedded Programming](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/makefile.html)
* [ATtiny using C](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_c.html)
* [ATtiny Embedded Programming with the Arduino IDE](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html)

From looking through this documentation I decided to program the board within the Arduino IDE to start with using the tutorial provided as it is was more familiar to me, although I plan to program my board using make in the future.

## Programming the board

Just like in [<u>Electronics Production</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/electronics-production/), once the board is soldered it needs to be programmed. The task was to make the button turn on/off the LED but the difference this week was that the FabISP would be used to program the board produced during [<u>Electronics Design</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/electronics-design/). I had less problems with the soldering with this week's board so a little re-soldering was required but this process was fairly quick and straightforward. I used [<u>this tutorial</u>](http://highlowtech.org/?p=1695) to program the board using the FabISP and the [<u>documentation</u>](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html) produced by the Fab Academy.

First of all I had to connect the two boards using some wires and clips that connect the 6 pin headers together.

![]({{site.baseurl}}/programming.jpg)

The board is programmed through using the Arduino IDE environment using Arduino code. Something to keep in mind with this code however is that the pinouts on the ATtiny 44A microcontroller are not the same number in the Arduino code. To use the FabISP board within the Arduino IDE it is necessary to download the correct library in order to use it, which can be found [<u>here</u>](https://github.com/damellis/attiny). I followed the tutorial once the library was installed but once I uploaded the code I received an error when 'burning to bootloader'.

![]({{site.baseurl}}/programmingerror.png)

After some debugging I realised that although I had set up the board, port and programmer correctly in the tools menu I had not selected the correct processor or clock.

![]({{site.baseurl}}/toolsnotsetupcorrectly.png)

I then did not have any problems burning to bootloader and could program the board using the code from the tutorial to make the LED turn on and off with the button.

![]({{site.baseurl}}/bootloadersuccess.png)

Within the code I had to find the part which defined the pin numbers in the 'blink' example and change the 'buttonPin' from 2 to 3 and ledPin from 13 to 7 due to the difference in pinouts between the ATtiny microcontroller and the Arduino. The code (file below) I used to turn the LED on and off is as follows:

![]({{site.baseurl}}/code.jpg)

<br>
<iframe src="https://player.vimeo.com/video/332316496" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

## Useful links

* [Data Sheet](http://academy.cba.mit.edu/classes/embedded_programming/doc8183.pdf)
* [Embedded Programming Tutorial](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week08_embedded_programming/attiny_arduino.html)
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week8/) (Embedded Programming)

## Files

* [LED Blink]({{site.baseurl}}/embedded-programming-sketch.ino) code
