---
title: Interfaces & Applications
period: 08-15 May 2019
date: 2019-01-24 12:00:00
term: 2
published: true
---
Interfaces and Applications was a week which combined different knowledge and skills picked up over the course of the Fab Academy such as inputs and outputs, but added the interfacing element. The assignment was to create an application that interfaces with an input and / or an output. This meant becoming familiar with an application in order for this interfacing to happening. The project for this week directly relates to the project from project #2 (WIFI communication) [<u>Network and Communications</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/networking-and-communications/) as it is a continuation of the [<u>ComPHOrtable School</u>](https://hackmd.io/s/ry9dAIhqE#About) project that was led by Santi and Xavi from the Fab Lab Barcelona's Future Learning Unit I was able to be part of for my masters project, which will be explained in this documentation.

To give a brief summary and recap of the project, the ComPHOrtable school project was formed of a series of workshops carried out in the Barcelona Fab Lab with a mixture of students and teachers in May 2019. It allowed participants to ideate and build a project that incorporated photonics, artificial intelligence and design to create a more inclusive school environment. The project was coordinated by [<u>The Institute of Photonic Sciences of Catalunya</u>](https://www.icfo.eu/) (ICFO).

The documentation for this week's assignment will document the interface and application aspect of the project, and include the information about machine learning which relates to my final project. As a result, the project created is the work of participants of the workshop which has been facilitated by Xavi & Santi of the Future Learning Unit and myself over two workshops lasting a period of 9 hours.

## Interfaces & Applications Overview

Xavi from the Fab Lab gave a class which gave an overview of the different applications we could use for the assignment including [<u>Processing</u>](https://processing.org), [Firefly](http://www.fireflyexperiments.com/) and [App Inventor](http://ai2.appinventor.mit.edu/). The class [<u>documentation</u>](https://hackmd.io/s/HJvhtz0oV) covered these programs and included lots of tutorials on each. The application I will be documenting is App Inventor as this was used in the workshop for the communication between a machine learning model and an LED ring. The class showed me the range of possibilities and projects with using just an Arduino and connecting it via serial to use in Processing and is something I would like to look into more to make interactive projects with different sensors and visuals.

## Creating a Project

During the class in order to create a project, Xavi explained a reflective exercise in order to set the parameters for the project and define what you want to do. The exercise was answering a series of questions. I will answer these questions from the perspective of the participants in the workshop, which will communicate what the project set out to do:

<u>What Do I Want to Do?</u>

I want to create a mobile application that can be used by students after a group activity which measures how much they enjoyed it through analysing what they wrote, changing the colour of a light sculpture at the front of the classroom according to what they wrote.

<u>What is my input?</u>

Text from students input into the mobile application.

<u>What is my output?</u>

LED ring that forms part of a light sculpture.

<u>How is the communication between the input and the output?</u>

Communication happens between the input and output happens through a mobile application created in App Inventor. The application that interfaces with an LED ring, through a machine learning model (text recognition, sentiment analysis) that has been trained using the [<u>Machine Learning for Kids</u>](https://machinelearningforkids.co.uk/) platform.

<u>Wired or wireless? Bluetooth? WIFI?</u>

The communication is wireless (WIFI), but could also be Bluetooth. Full documentation on how this communication works can be found [<u>here</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/networking-and-communications/).

Different techniques were used in the ideation process to generate ideas for the project:

![]({{site.baseurl}}/design-thinking-interface.jpg)

![]({{site.baseurl}}/sketching-project-interface.jpg)

## Pre-requisites

Some knowledge of machine learning was necessary for this project which was covered in the first workshop. This included creating a machine learning model on the [<u>Cognimates</u>](http://cognimates.me/home/) platform and introducing photonics through a project using Scratch and the BBC Micro:bit. I think the workshop was a good introduction to combining machine learning and physical computing and was a good base to go off for the second workshop where the teachers had to create their own models and start prototyping ideas.

{% figure caption: "*Student learning about machine learning and physical computing using Scratch and the BBC Micro:bit in the first workshop*" %}
![]({{site.baseurl}}/microbit-scratch-ml4k.jpg)
{% endfigure %}

I created an adapted smaller workshop for the mid-term presentation for the third term, which involved creating a text sentiment analysis machine learning model and linking it to the Micro:bit with the idea that people could have a go themselves during the presentation. Here is the presentation which covers some basic knowledge and the exercise:

<br>
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRTqmjXsTGxdQrttN-7bJ8glkI4c9qemTO1o0Px1h8dbtSINROl9S-jZyv-dD5KTTRvqs6Ycwz2lpB_/embed?start=false&loop=false&delayms=3000" frameborder="0" width="640" height="360" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## App Inventor (Application)

As mentioned the application selected for the project was [<u>App Inventor</u>](http://ai2.appinventor.mit.edu/). Like Scratch, App Inventor is a block-based programming language developed in MIT that is very accessible and is a great entry to start building mobile applications (Android only) quickly. It was selected in the context of the ComPHOrtable school project as a way to link the machine learning model created in Machine Learning for Kids to the physical world, because models can exported to App Inventor (as well as Scratch and Python). It works in the browser and you just need to install the app from the Google Play to get started.

I had never used it before so took some time to work out how the software works and how to link it to a machine learning model (more on this below). There are a number of tutorials on the [<u>documentation</u>](https://fabacademy.org/2019/labs/barcelona/local/wa/week15/) to get started and found some to get started. Getting used to the interface was quite straightforward and within 30mins to an hour had an idea of how it worked and could start prototyping with it. There are two main spaces: the 'design' space, where you design the interface of the application, and the 'block' space where you build the app itself. This is where the block-programming happens, so was quite familiar with this type of programming through using Scratch. Here are some of the tutorials I found relating to IoT:

* [App_Inventor IoT Setup](http://iot.appinventor.mit.edu/assets/tutorials/MIT_App_Inventor_IoT_Setup.pdf)
* [App Inventor IoT Starter Tutorial](http://iot.appinventor.mit.edu/assets/tutorials/MIT_App_Inventor_IoT_Starter_Tutorial.pdf)
* [App Inventor Basic Bluetooth Setup](https://drive.google.com/file/d/0B51cwz24uqobanJ2XzhLbGpQOFk/view)

I also found this tutorial which gives an introduction to the overall interface:
<br>
<iframe width="640" height="360" src="https://www.youtube.com/embed/6hAAznJqPLk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Machine Learning & Text Analysis (Input)

The input for the project is the text that students write in the application. A machine learning model is used so it can analyse the text, and make a prediction based on the data it was trained on. This means that the students don't have to write an exact phrase in order for the output to respond, as the model can interpret it. In previous workshops we had used Machine Learning for Kids, and this was the platform most suitable for the project as the model could be exported to App Inventor. In order to do this you will have to create an account and generate some API keys to connect with the App Inventor extension.

On the website there are a number of [<u>worksheets</u>](https://machinelearningforkids.co.uk/#!/worksheets) with a range of different activities to learn about machine learning and prototype some projects. I would recommend exporting the models to Scratch to begin with as there is the most documentation to support these projects and is the most straightforward.

You have to create an account, which means you can start building projects and saving the progress of your students if you are a teacher. The steps of creating a machine model can be summarised as:

* Choose your labels
* Build your dataset
* Train your model
* Test your model

I created a project in my account and called it 'How was workshop?' and went through the process of training a model.

![]({{site.baseurl}}/ml4k-projects.png)

Once the project is created you need to add labels (for this example good and bad) and start inputting data to train the model.

![]({{site.baseurl}}/training-data-ml4k.png)

Once you have added enough examples to your labels (the more the better) then you can train the model, which usually takes a couple of minutes. The Machine Learning for Kids platform uses the IBM Watson Assistant to train the model. Once you have trained it you can test it and see how it responds to your text. If you are not happy with it you can add or delete data and retrain the model until you are happy with it.

![]({{site.baseurl}}/training-data-2-ml4k.png)

In the first workshop participants learnt about this process using the [<u>uClassify</u>](https://www.uclassify.com/) text classification via Cognimates. Here is a result of the testing phase of a model they trained. The text says 'I didn't loose so I'm still rich and happy' with a confidence score of 50% which I thought was an interesting result and showed they were engaged in the activity.

![]({{site.baseurl}}/machine-learning-model-training-interface.jpg)

## Adafruit NeoPixel (Output)

During the course of the workshop I learnt a lot about the Adafruit NeoPixel and the products in its range. For this project a ring with 8 LEDs was used. Initially code was created with the BBC Micro:bit in its browser-based the [<u>coding environment</u>](https://microbit.org/code/).

![]({{site.baseurl}}/neopixel-microbit-interface.jpg)

In a following workshop students created a project using the NeoPixel strips:

![]({{site.baseurl}}/neopixel-strip-interface.jpg)

## The Project - Team Lamp

I will now describe how the different elements in the project come together to create the final outcome. This part of the documentation will focus on the design of the application as the setup of the LEDs was described in [Network and Communications](https://mdef.gitlab.io/oliver.juggins/fabacademy/networking-and-communications/), which contains all the documentation on WIFI communication for the project.

Requirements:

* x1 ESP8266 (or ESP32)
* x1 NeoPixel Ring (8) LED ring
* x1 Breadboard
* x3 Cables
* Laser cut structure
* Mobile phone
* Machine Learning model
* Mobile Application (created in App Inventor)

Although the project was initially prototyped with a Micro:bit, the microcontroller was changed to an ESP8266 for WIFI communication and to link it more easily to App Inventor.

To better communicate the project, here is an overview of the different elements:

![]({{site.baseurl}}/project-components-applications.png)

The first step is to export the machine learning model from Machine Learning for Kids to App Inventor. This is done through pressing the 'make' and then choosing App Inventor.

![]({{site.baseurl}}/make-ml-interface.jpg)

![]({{site.baseurl}}/make-ml-app-inventor-interface.jpg)

You are then given instructions on how to import the extension into App Inventor. This took a couple of attempts and can be done by downloading the extension onto your computer or using the link provided.

![]({{site.baseurl}}/ml4k-app-inventor.png)

In order to add the Machine Learning for Kids extension in App Inventor go to 'import' section in the 'Palette' section on the left hand side. You then can paste in the link generated from Machine Learning for Kids or download the extension onto your computer and upload it.

![]({{site.baseurl}}/ml-import-extension-interface.png)

There is not a lot of documentation for using Machine Learning for Kids but there is a [<u>GitHub</u>](https://github.com/kylecorry31/ML4K-AI-Extension) page which I referred to in order to get started.

After reading the documentation on GitHub I started adding in the different elements needed, building the application from scratch. I added in the type of classification the model used and the API Key.

![]({{site.baseurl}}/attempt-building-from-scratch.png)

There are some [<u>example projects</u>](https://github.com/kylecorry31/ML4K-AI-Extension/tree/master/examples) to look at which I found really useful, and used this as a base to build the project on. There was also the design for the app in the 'design' part of the editor which I played around with a bit to try and understand.

![]({{site.baseurl}}/example-interface-make-me-happy.png)

I looked at the example code and started using my phone to see how the app responded and how the setup worked. On the menu go to 'Connect' - 'AI Companion' and a QR code should be generated that you can scan to make the connect and use the application on your phone. If it doesn't connect properly you can reset the connection and try again as it doesn't always work.

![]({{site.baseurl}}/prototyping-app-ml4k-interface.jpg)

This is what the first version of the interface looked like. Students could write a response and choose a number which related to the LED value.

![]({{site.baseurl}}/interface-iteration.jpg)

This is what the code for the example 'make me happy' project on the Machine Learning for Kids platform looked like, and used it as the data required and labels were similar to the ComPHOrtable school project requirements.

![]({{site.baseurl}}/example-code-make-me-happy.png)

The highlighted area shows where you have to paste in your API key.

![]({{site.baseurl}}/example-code-make-me-happy-highlighted.png)

After trying to get this code to work and connecting it with my phone various times, I kept on getting an error message saying that the server could not connect. I realised I was using the wrong API key (you should paste the one generated when exporting from Machine Learning for Kids). There was also a place I did not know I had to paste this API key - which was in the 'designer' section of the program. You just have to click on the icon from Machine Learning for Kids in 'components' and you can paste in the key.

![]({{site.baseurl}}/ml-api-key-interface.png)

![]({{site.baseurl}}/blocks-final-code.png)

The next part of the code to add was the WIFI. Here is the section of the code which relates to the WIFI with the different 'teamstates'. These different conditions were the different options that the LED ring could take (positive, negative and neutral). In order to connect the application using WIFI you have to paste in the IP address which is generated in the serial port from the ESP8266 sketch. Note that it is 'http' not 'https' so you will have to remove the 's'. In order to test it and for the application to work your mobile has to be on the same WIFI as defined in the sketch for the NeoPixel.

![]({{site.baseurl}}/zoom-in-blocks-highlighted.png)

Once the machine learning model was connected to the application, putting the correct API key in the right place, and the different conditions had been set the user interface had to be designed. This is the deign that Xavi did that has a text prompt for the student to give their feedback on the activity:

![]({{site.baseurl}}/app-interface.png)

In order to connect you then have to scan the QR code that is generated, and you can try the app out. Here are screenshots of me testing the app.

![]({{site.baseurl}}/scanning-phone.png)

![]({{site.baseurl}}/testing-prototype-interface.png)

Xavi created the final code for the project which I had a look at after the project to try and understand what was happening. There are 3 blocks, or segments of code: one for the model, to make a classification with the different conditions or 'teamstates'. The other is for when there is an error and the last on is for the button, which when clicked makes the API call to classify the text.

![]({{site.baseurl}}/blocks-final-code.png)

This is a video Xavi made to show the project in action:
<br>
<iframe src="https://drive.google.com/file/d/1G_AKjQ4lchELDUi_QT3HGihEJoQbJZQa/preview" width="640" height="480"></iframe>
<br>
This is another video from another workshop in the same series. This time students designed the project which was a system based around getting people into groups based on their characteristics, so that groups would be balanced. A machine learning model analyses a description students have to give of themselves, and assigns them a colour. The teacher then chooses the size of groups based the activity and the tables change colours. The student then goes to the table with their colour, forming the groups.
<br>
<br>
<iframe src="https://player.vimeo.com/video/340194600" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<br>
![]({{site.baseurl}}/table-project-interface.jpg)

## Final Reflection

This was a week I learnt a lot and was happy to be able to link the assignment to my final project. Making the link between the physical world and real-world projects with machine learning has been a large development for my main project, and is thanks to the workshops I was able to take part in and through learning about App Inventor. As a result, I learnt more than just the technical side of the making and application, but the bigger picture in how these different elements can combine through OTA communication and connect to a machine learning model.

I plan to carry on using App Inventor for future workshops as it is such a powerful tool in being able to bring machine learning to the real world and help children prototype projects in a matter of hours and countless other interactive IoT projects. There appears to be quite an active community around the project, with an [<u>App of the Month</u>](https://appinventor.mit.edu/explore/app-month-gallery.html) competition for children to participate in.

There are many possibilities for future projects now that I know how to connect the ESP8266 to App Inventor via WIFI and connect to different actuators and sensors. I am thinking of making a prototype of the project, or one similar to the one I proposed in [<u>Computer-Aided Design</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/computer-aided-design/), and then try and create a workshop based around creating acoustic music machines. Another  possibility that could link to interactive interfaces and applications that link machine learning could be through using artificial intelligence hardware from Seeed studio that have recently been released (May 2019). [<u>Here</u>](https://www.seeedstudio.com/artificial-intelligence-hardware?utm_source=facebookads&utm_medium=ads&utm_campaign=artificial-intelligence-hardware&fbclid=IwAR23EomJr_UsegINeNjo-s7VukZ3qUY-Og6HmFdC2j4jfchQykcqCFMwwcg) is some information on their AIoT boards.

## Useful links

* [The Institute of Photonic Sciences of Catalunya](https://www.icfo.eu/)
* [Network and Communications](https://mdef.gitlab.io/oliver.juggins/fabacademy/networking-and-communications/) Documentation
* [Machine Learning for Kids](https://machinelearningforkids.co.uk/)
* [Cognimates](http://cognimates.me/home/)
* [uClassify](https://www.uclassify.com/) Text Classification
* [App of the Month](https://appinventor.mit.edu/explore/app-month-gallery.html) from App Inventor
* Artificial Intelligence Hardware from [Seeed Studio](https://www.seeedstudio.com/artificial-intelligence-hardware?utm_source=facebookads&utm_medium=ads&utm_campaign=artificial-intelligence-hardware&fbclid=IwAR23EomJr_UsegINeNjo-s7VukZ3qUY-Og6HmFdC2j4jfchQykcqCFMwwcg)
* [ComPHOrtable School](https://hackmd.io/s/ry9dAIhqE#About) Project Documentation
* [Fab Academy BCN Documentation](http://fab.academany.org/2019/labs/barcelona/local/wa/week15/) (Interfaces & Applications)

## Files

Visit [<u>Network & Communications</u>](https://mdef.gitlab.io/oliver.juggins/fabacademy/networking-and-communications/) for the code and files for the Adafruit NeoPixel and WIFI communication. Note the IP address generated will have to be pasted into the project downloaded below.

* [Team Lamp]({{site.baseurl}}/ComPHOrtable_Schhols_Lamp_App.zip) (App Inventor)

<!-- Really happy to have learnt about App Inventor - really good tool for making interactive IoT projects. Whilst researching and learning about it came across lots of different projects which are inspiration. Community around the project - App of the month.

https://appinventor.mit.edu/explore/app-month-gallery.html

Whole ethos of the app is about empowering young people to make projects that mean something to them, so really like the whole idea behind it. Can even put you app on Google Play.

Such good experience not just for Fab Academy and the skills I picked up but learning a lot about workshop design and picking up more tools that can be used in future workshops. Never used App Inventor before, but is perfect for kids, and using an App to type in stuff, connecting to an ML model to change different sensors is really cool!

reflection - able to build an app, that interacts with photonics and machine learning using wifi, and prototype something in a matter of a couple of sessions. Amazing! And the fact it is understandable by children! Would not be quick using proper code. Really good experience using App Inventor, and will definately incorporate into more workshops and build projects around it.

Can build a series of workshops around these different tools.

Whole idea is that through these workshops, ML is having an application which they designed, and a project that means something to them in the real world. This is the aim of my project, to now only pass on knowledge about how ML works and why people should learn about it, but present it as a tool which they can make personalised projects. In this sense it has been a great experience, as I was able to learn some new software and learn how machine learning can interact with the real world, in a project that people with no prior knowledge can also be part of and learn!

Want to try and design more AIOT (artificial intelligence internet of things activities).


Want to try and prototype something with some more boards from Seeed Studio (link).
https://www.seeedstudio.com/artificial-intelligence-hardware?utm_source=facebookads&utm_medium=ads&utm_campaign=artificial-intelligence-hardware&fbclid=IwAR23EomJr_UsegINeNjo-s7VukZ3qUY-Og6HmFdC2j4jfchQykcqCFMwwcg -->
